require('es6-promise').polyfill();

var fs = require("fs");
var path = require("path");
var postcss = require("postcss");
var atImport = require("postcss-import");
var cleanCSS = require('clean-css');
var url = require("postcss-url");

var resolverRules = require('./postcss-path-resolver');

//all entry points
var entryPoints = [
    'admin-console.css',
    'operator-console.css',
    'display-node.css',
    'ticket-console.css'
    ];


var output = './dist/styles/';

// process css
entryPoints.forEach(function(file) {
    var css = fs.readFileSync('./src/styles/' + file, 'utf8');

    postcss()
        .use(atImport({
            // path: ["src/styles"],
            resolve: resolverRules
        }))
        // .use(url())
        .process(css, {
            // `from` option is required so relative import can work from input dirname
            from: file
        })
        .then(function (result) {
            var resultCSS = new cleanCSS().minify(result.css).styles;

            fs.writeFile(output + file, resultCSS, function(err) {
                if (err) {
                    console.error('Error: ', err);                    
                } else {
                    console.log('styles: ' + output + file);
                }
            });
        });
});
