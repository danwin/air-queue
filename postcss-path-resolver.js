const aliases = {
    'node_modules': './node_modules'
};
var path = require("path");

module.exports = function (id, basedir) {
    // resolve alias @blabla, @import '@blahbla/style.css'; same as version 1

    for (var alias in aliases) {
        if (new RegExp('^'+alias).test(id)) {
            // console.warn('********************. alias found, ', id, path.resolve('./node_modules', id.slice(alias1.length + 1)));
            return path.resolve(aliases[alias], id.slice(alias.length + 1))
        }    
    }

    // resolve relative path, @import './components/style.css'; prefix with './' or '../'
    if (/^\./.test(id)) {
        // console.warn('***********. relative path found', id, basedir);
        id = id.replace(/[\.]+\//gi, '');
        basedir = [basedir, 'src', id].join('/');
        // console.warn('***********>>>. ', basedir);
        return basedir;
    }

    // resolve node_modules, @import 'normalize.css/normalize.css'
    // console.warn('************. node_modules path found', id, path.resolve('./node_modules', id));
    return path.resolve('./node_modules', id)
}