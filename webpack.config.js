const webpack = require('webpack');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const path = require("path");

// Small trick how to intercept environment in webpack 4:
module.exports = (env, argv) => {
  console.log('Mode: ', argv.mode)        // outputs development
  var conf = {
    entry: {
      "ticket-console": "./src/ticket-console.js",
      "admin-console": "./src/admin-console.js",
      "operator-console": "./src/operator-console.js",
      "display-node": "./src/display-node.js"
    },
    output: {
      path: path.resolve(__dirname, "dist"),
      // path: "dist",
      filename: "[name].js"
      // publicPath: "/"
    },
    module: {
      rules: [
        // pug, jade -> html
        {
          test: /\.pug$/,
          use: ["pug-loader"]
        },
        //  JS
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: ["source-map-loader"],
          enforce: "pre"
        },
        {
          test: /\.html$/,
          use: [
            {
              loader: "html-loader",
              options: { minimize: true }
            }
          ]
        },
        // CSS - today we`re using postcss script as much convenient (I don know any simple solutions to map each entry to separate css file)
        {
          test: /\.(sa|sc|c)ss$/,
  
          // // use: [
          // //   devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
          // //   'css-loader'
          // //   ,'postcss-loader'
          // //   // ,'sass-loader'
          // // ],
  
          // // use: ExtractTextPlugin.extract({
          //   use: [
          //     {
          //       loader: 'css-loader',
          //       options: {
          //         importLoaders: 1,
          //         // alias: {
          //         //   "../app": "app"
          //         // }
          //       }
          //     },
          //     {
          //       loader: 'postcss-loader',
          //       options: {
          //         config: {
          //           path: './postcss.config.js'
          //         }
          //       }
          //     }
          //   ]
          // // })
  
        }
      ]
    },
    plugins: [
      // Each HTML entry starts its own instance of plugin:
      new HtmlWebPackPlugin({
        template: "./src/index.pug",
        filename: "./index.html"
      }),
      new HtmlWebPackPlugin({
        template: './src/ticket-console.pug',
        filename: './ticket-console.html',
        chunks: ['ticket-console']
      }),
      new HtmlWebPackPlugin({
        template: './src/admin-console.pug',
        filename: './admin-console.html',
        chunks: ['admin-console']
      }),
      new HtmlWebPackPlugin({
        template: './src/operator-console.pug',
        filename: './operator-console.html',
        chunks: ['operator-console']
      }),
      new HtmlWebPackPlugin({
        template: './src/display-node.pug',
        filename: './display-node.html',
        chunks: ['display-node']
      }),
      // new HtmlWebPackPlugin({
      //   filename: 'main-display-node.html',
      //   template: 'src/main-display-node.pug',
      //   // chunks: ['exampleEntry']
      // }),
      
    ]
  };

  if (argv.mode === 'production') {
    // PROD mode:
    conf.plugins.unshift(
      new CleanWebpackPlugin(['dist/*.*', 'dist/styles/*.*'])
    );

    conf.plugins.push(new UglifyJsPlugin({
        test: /\.js($|\?)/i,
        uglifyOptions: {
            output: {
                comments: false
            },
            compress: {
              ecma: 6,
              booleans: true,
              warnings: false,
              drop_console: false
            }
        }
    }));

  } else {
    // DEV mode
    conf.devtool = "source-map";
    conf.devServer = {
      // proxy: { // proxy URLs to backend development server
      //   '/api': 'http://localhost:3000'
      // },
      contentBase: path.join(__dirname, 'dist'), // boolean | string | array, static file location
      // compress: true, // enable gzip compression
      // historyApiFallback: true, // true for index.html upon 404, object for multiple paths
      hot: true, // hot module replacement. Depends on HotModuleReplacementPlugin
      https: false, // true for self-signed, object for cert authority
      noInfo: true, // only errors & warns on hot reload
    }
    conf.plugins.push(new webpack.HotModuleReplacementPlugin());
  }

  return conf;
}
