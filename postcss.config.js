const path = require('path');
const resolverRules = require('./postcss-path-resolver');

module.exports = {
    plugins: {
        'postcss-import': {
            resolve: resolverRules
            // resolve: function (id, basedir) {
            //     // resolve alias @blabla, @import '@blahbla/style.css'; same as version 1
            //     const alias1 = '#alias';
            //     if (new RegExp('^'+alias1).test(id)) {
            //         console.warn('alias found, ', id, path.resolve('./pathToBlah', id.slice(alias1.length + 1)));
            //         return path.resolve('./pathToBlah', id.slice(alias1.length + 1))
            //     }
        
            //     // resolve relative path, @import './components/style.css'; prefix with './' or '../'
            //     if (/^\./.test(id)) {
            //         console.warn('relative path found', id, path.resolve(basedir, id));
            //         return path.resolve(basedir, id)
            //     }
        
            //     // resolve node_modules, @import 'normalize.css/normalize.css'
            //     console.warn('node_modules path found', id, path.resolve('./node_modules', id));
            //     return path.resolve('./node_modules', id)
            // }
        },
      'postcss-url': {url: 'inline'},
      'postcss-cssnext': {
        browsers: ['last 2 versions', '> 5%'],
      },
    },
  };

// module.exports = {
//     plugins: [
//         require('precss'),
//         require('autoprefixer')
//     ]
// }

// require('es6-promise').polyfill();

// var fs = require("fs");
// var path = require("path");
// var postcss = require("postcss");
// var atImport = require("postcss-import");
// var cleanCSS = require('clean-css');

// //all entry points
// var entryPoints = [
//     'styles.css',
//     'bundle.index.css',
//     'bundle.filter.css'
//     ];


// var output = './app/static/styles/';

// // process css
// entryPoints.forEach(function(file) {
//     var css = fs.readFileSync('./app/static/css/' + file, 'utf8');

//     postcss()
//         .use(atImport({
//             path: ["./app/static/css"]
//         }))
//         .process(css, {
//             // `from` option is required so relative import can work from input dirname
//             from: file
//         })
//         .then(function (result) {
//             var resultCSS = new cleanCSS().minify(result.css).styles;

//             fs.writeFile(output + file, resultCSS, function(err) {
//                 console.log(err ? err : 'styles: ' + output + file);
//             });
//         });
// });
