// controllable.js

var firebase = require('firebase');
var Magneto = require('./magneto');
var IPersistent = require('./persistent');

var thisModName = 'IControllable';

// Export:
Magneto[thisModName] = IControllable;

var _extend = Magneto.extend;

/**
 * Implements 2-way command channel over IPersisten interface
 * @param {object} self Instance Magneto (defined)
 * @param {object} data Optional values for attributes
 */
function IControllable(self, data) {
	data = data || {};

	// Implements interface "Authentic":
	IPersistent(self, data);

	// Do not apply interface twice!:
	if (self[thisModName]) return;
	self[thisModName] = true;

	var clearCommand = function () {
		return self.database
			.ref(self.instanceDataPath())
			.child('control/request')
			.set(null)
	}

	_extend(self, {
		/**
		 * Positive response on command
		 * @param {number} timestamp UTC Time (number of milliseconds) when command is handled
		 */
		'ACK': function (timestamp, result) {
			var targetRef = self.database
				.ref(self.instanceDataPath())
				.child('control/response')
			console.log('client: writhing ACK')
			result = result || 'Ok';
			// Clear NACK flag in db record:
			targetRef.child('NACK').set(null).catch(Magneto.handleException)
			// Write ACK flag to db record
			return targetRef.child('ACK').set({
				'timestamp'	: timestamp,
				'result'	: result
			}).catch(Magneto.handleException)
		},

		/**
		 * Negative reponse.
		 * @param {string} errorMessage Сообщение об ошибке, необязателен.
		 */
		'NACK': function (timestamp, details) {
			var targetRef = self.database
				.ref(self.instanceDataPath())
				.child('control/response')
			details = details || null;
			targetRef.child('ACK').set(null).catch(Magneto.handleException)
			return targetRef.child('NACK').set({
				'timestamp'	: timestamp,
				'details'	: details
			}).catch(Magneto.handleException)
		},

		'listenCommandsChannel': function (channelName, callback, eventFilter) {
			eventFilter = eventFilter || 'value';
			self.listenToChildNode (channelName, callback, eventFilter)
		},

		'installCommandDispatcher': function (commandDispatcher) {
			var dispatcher = commandDispatcher

			var callback = function (snapshot) {
				var buffer = snapshot.val();
				if (buffer) {
					// var timestamp = (new Date()).getTime();
					var command = buffer.command;
					var args = buffer.args;
					var timestamp = buffer.timestamp || null;
					console.log('COMMAND detected:', command, args)
					if (command) {
						try {
							if (dispatcher(command, args))
								self.ACK(timestamp, {'command': command})
									.then(clearCommand);

						} catch (e) {
							self.NACK(timestamp, {'command': command, 'error': e })
								.then(clearCommand)
						}
					}
				}
			}

			self.listenToChildNode ('control/request', callback, 'value')
		}

	})
}

module.exports = IControllable;