// pnp.js
var Magneto = require('./magneto');
var IPersistent = require('./persistent');
var IStateful = require('./stateful');

var thisModName = 'IPnP';

var raise = Magneto.raise;

// Export:
Magneto[thisModName] = IPnP;

var POSSIBLE_STATES = {
	'state-pnp-connecting': true,
	'state-pnp-done': true,
	'state-pnp-new-address': true,
	'state-pnp-address-conflict': true,
	'state-pnp-bus-overflow': true,
	'state-pnp-unknown-error': true
}

// Magneto.constants['PNP_DEVICE_NOT_INITIALIZED'] = ++Magneto.constants.lastValue
Magneto.constants['PNP_OK_BINDING_EXISTS'] = ++Magneto.constants.lastValue
Magneto.constants['PNP_OK_BINDING_CREATED'] = ++Magneto.constants.lastValue
Magneto.constants['PNP_BUS_OVERFLOW'] = ++Magneto.constants.lastValue
Magneto.constants['PNP_DEVICE_ADDR_CONFLICT'] = ++Magneto.constants.lastValue
Magneto.constants['PNP_NEEDS_NEW_ADDR'] = ++Magneto.constants.lastValue

var _extend = Magneto.extend;
var _invert = function (object) {
	var result = {};
	for (var key in object) {
		if (object.hasOwnProperty(key)) {
			var value = object[key];
			if (typeof value === 'number' || typeof value === 'string') {
				console.log('_invert: ', key, value)
				result[value] = key;
			} else raise ('_invert error: value is not a valid key: '+ typeof value)
		}
	}
	return result;
}
var _assertDefined = function (value, errMessage) {
	if (value !== void 0) return value;
	raise('_assertDefined: '+errMessage)
}

var numDigits = 4;

var UUID = function () {
	// Implementation suggested: http://stackoverflow.com/a/2117523
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		return v.toString(16);
	}).toUpperCase();
}

var R4 = function (argument) {
	// body...
}

/**
 * Implements PnP functions
 * @param {object} self Instance Magneto (defined)
 * @param {object} data Optional values for attributes
 */
function IPnP(self, data) {
	data = data || {};

	data.POSSIBLE_STATES = POSSIBLE_STATES;
	IPersistent(self, data);
	IStateful(self, data)

	// New properties
	_extend(self, {
		'deviceType': data.deviceType || 'basic-device',
		'busDirectory': data.busDirectory || 'bus',
		'regDirectory': data.regDirectory || 'registry',
		'storageKeyForDeviceUUID': 	data.storageKeyForDeviceUUID || 'MagnetoDevUUID',
		'storageKeyForDeviceAddr': 	data.storageKeyForDeviceAddr || 'MagnetoDevAddr',
		'deviceAddr': null,
		'maxAddrValue': data.maxAddrValue || Number.MAX_VALUE,
		'_deviceUUID': data.deviceId || null
		// 'deviceType': null
	})

	// Do not apply interface twice!:
	if (self[thisModName]) return; //[] to-do: allow apply "data" multiple times!
	self[thisModName] = true;

	var getBusRef = function () {
		console.log('---> getBusRef', self.database.ref(self.busDirectory + '/' + self.deviceType).toString())
		return self.database.ref(self.busDirectory + '/' + self.deviceType)
	}
	var getRegistryRef = function () {
		console.log('---> getRegistryRef', self.database.ref(self.regDirectory + '/' + self.deviceType).toString())
		return self.database.ref(self.regDirectory + '/' + self.deviceType)
	}
	var getDataRef  = function () {
		console.log('---> getRegistryRef', self.database.ref(self.dataDirectory).toString())
		return self.database.ref(self.dataDirectory)
	}

	/*
	Registry and bus manipulation
		*/

	var readRegistryValue = function () {
		return new Promise(function (resolve, reject) {
			console.log('---> readRegistryValue', getRegistryRef().child(self.getUUID()).toString())
			getRegistryRef().child(self.getUUID()).once('value', function (snapshot) {
				// resolve with extended info
				resolve(snapshot.val())
			})
			.catch(reject)
		})
	}

	var readBus = function () {
		return new Promise(function (resolve, reject) {
			console.log('===>readBus', getBusRef().toString())
			getBusRef().once('value', function (snapshot) {
				resolve(snapshot.val())
			})
			.catch(reject)
		})
	}


	var writeRegistryValue = function (record) {
		return new Promise(function (resolve, reject) {
			console.log('---> writeRegistryValue', getBusRef().child(self.getUUID()).toString())
			getRegistryRef().child(self.getUUID()).set(record).then(resolve).catch(reject)
		})
	}

	var updateAddressInRegistry = function (addr) {
		var currentAddr = addr;
		console.log('updateAddressInRegistry', currentAddr)
		return getRegistryRef().child(self.getUUID()).child('ADDR')
			.set(currentAddr)
			.then(function () {
				return currentAddr
			})
	}

	var updateAddressInData = function (addr) {
		var currentAddr = addr;
		console.log('updateAddressInData', currentAddr)
		return getDataRef().child(self.getUUID()).child('ADDR')
			.set(currentAddr)
			.then(function () {
				return currentAddr
			})
	}

	var setAddress = function (deviceAddr) {
		// if (!deviceAddr) raise('setAddress: invalid addr:' + deviceAddr)
		self.deviceAddr = deviceAddr;
		return deviceAddr
	}

//---

	// New methods:
	_extend(self, {
		'_readLocal': function (key) {
			return localStorage.getItem(self.deviceType + '/' + key)
		},
		'_writeLocal': function (key, value) {
			localStorage.setItem(self.deviceType + '/' + key, value)
		},

		'getKey': function () {
			return self.getUUID()
		},

		'getUUID': function () {
			if (self._deviceUUID === null) {
				self._deviceUUID = self._readLocal(self.storageKeyForDeviceUUID)
			}
			return self._deviceUUID || null;
		},

		'getAddress': function () {
			return self.deviceAddr || null;
		},

		'generateDeviceUUID': function () {
			// to-do: remove old record if any!
			self._deviceUUID = UUID();
			console.log('*generateDeviceUUID', self.storageKeyForDeviceUUID, self._deviceUUID)
			self._writeLocal(self.storageKeyForDeviceUUID, self._deviceUUID)
			return self._deviceUUID
		},

		'initDevice': function () {
			var BUS_OVERFLOW = -1;

			console.log("Starting PnP device...")
			console.log('device type: ', self.deviceType)
			console.log('PnP bus:', self.busDirectory)
			console.log('Registry:', self.regDirectory)

			self.setState('state-pnp-connecting')

			var findFreeAddr = function (bus) {
				var addr = 1;
				bus = bus || {}; // <- if bus was not created yet
				while (bus[addr] && addr<self.maxAddrValue) addr++;
				return (addr === self.maxAddrValue) ? BUS_OVERFLOW : addr
			}
			var useAddr = function (addr) {
				if (!addr) raise('useAddr: invalid addr'+addr)
				return new Promise(function (resolve, reject) {
					function makeAttempt (newAddr) {
						if (newAddr === BUS_OVERFLOW) {
							reject(BUS_OVERFLOW)
							return
						}
						if (typeof newAddr !== 'number') {
							reject(false) // to-do: catch case like "Error: disconnect", etc...
						}

						console.log('useAddr', getBusRef().child(newAddr).toString())

						getBusRef().child(newAddr)
							.transaction(function (uuid) {
								if (uuid === null) return self.getUUID()
							}, void(0), false)
							.then(function (result) {
								console.log('TRANSACTION:', result)
								if (result.committed) {
									resolve(newAddr)
								} else {
									// else try next ADDR
									newAddr++;
									return Promise.reject( (newAddr < self.maxAddrValue) ? newAddr : BUS_OVERFLOW )
								}
							})
							.catch(makeAttempt)
					}
					makeAttempt(addr)
				})
			}

			var validateRegistryValue = function (record) {
				if (record === null) {
					// record does not exist, restore it...
					return writeRegistryValue( {'created': +(new Date()) } )
				} else {
					return record;
				}
			}

			/**
			 * [registerInBus description]
			 * @return Promise{ADDR} [description]
			 */
			var registerInBus = function () {
				var updateState = function (result) {
					self.setState('state-pnp-done')
					return result // <-- for subsequent .then()
				}
				return readBus()
					.then(findFreeAddr)
					.then(useAddr)
					.then(setAddress)
					.then(updateState)
					.catch(function (reason) {
						self.setState(
							(reason === BUS_OVERFLOW)
							? 'state-pnp-bus-overflow'
							: 'state-pnp-unknown-error')
						console.error('registerInBus: ', reason)
					})
			}

			var validateBusRecord = function () {
				return readBus()
					.then(function (bus) {
						var reverseLookup = _invert(bus);
						var addr = reverseLookup[self.getUUID()];
						if (addr) {
							console.warn('reverseLookup: record found', reverseLookup);
							setAddress(addr);
							self.setState('state-pnp-done')
							return addr
						} else {
							console.warn('reverseLookup: record NOT found', reverseLookup);
							return registerInBus() // <- returns Promise<addr>
						}
					})

			}

			return new Promise(function(resolve, reject){

				// [1] Check UUID exists;
				if (self.getUUID() === null) {
					self._deviceUUID = self.generateDeviceUUID();
					// Reset ADDR
					setAddress(null);
					// Find empty ADDR slot in a bus:
					registerInBus()
						.then(updateAddressInRegistry)
						.then(updateAddressInData)
						.then(resolve)
						.catch(reject)
				} else {
					// [2] UUID exists
					// [2.1] Check whether it exists in registry:
					readRegistryValue()
						.then(validateRegistryValue)
						.then(validateBusRecord)
						.then(updateAddressInRegistry)
						.then(updateAddressInData)
						.then(resolve)
						.catch(reject)
				}
			})
		},

		/*
		Add observer to own record in Bus ???
			*/


	})


	// self.listenToAuthState(self.connect)
	self.listenToAuthState(function (user) {
		if (user && !self.deviceAddr) self.initDevice().catch(Magneto.handleException)
	})


}

module.exports = IPnP;