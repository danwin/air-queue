// Reflects its presence in a system
var firebase = require('firebase');
var Magneto = require('./magneto');
var IPersistent = require('./persistent');
var IStateful = require('./stateful');

var thisModName = 'IOnline';

// Export:
Magneto[thisModName] = IOnline;


function IOnline(self, data) {
	data = data || {}

	// Do not apply interface twice!:
	if (self[thisModName]) return;
	self[thisModName] = true;

	var _isInstalled = false; // <-- flag which allows to install only once;
	// Internal state: whether instance record exists, to prevent creation of record for unregistered device:
	var _instanceRecordExists = false;

	// Implements "Persistent" interface:
	IPersistent(self, data)
	IStateful(self, data)

	self.POSSIBLE_STATES['state-online'] = true;
	self.POSSIBLE_STATES['state-offline'] = true;

	var observeInstanceRecordPresence = function (snapshot) {
		var collection = snapshot.val();
		_instanceRecordExists = (collection && collection[self.getKey()])
	}

	var installPresenceMonitor = function (user) {
		// Install only once and only when user !== null:
		if (_isInstalled || !user) return;

		var targetRef = self.database.ref(self.instanceDataPath()).child('statistics');
		var myConnectionsRef = self.database.ref(self.instanceDataPath()).child('online');

		console.log('ONLINE monitor', targetRef.toString(), myConnectionsRef.toString())

		// stores the timestamp of my last disconnect (the last time I was seen online)
		var lastOnlineRef = targetRef.child('lastOnline');

		var connectedRef = self.database.ref('.info/connected');
		connectedRef.on('value', function(snap) {
			if (snap.val() === true) {

			// ONLINE:

			if (self._lastState) self.setState(self._lastState);

			// Allow to write only to existing record of instance:
			// if (_instanceRecordExists) {

				// We're connected (or reconnected)! Do anything here that should happen only if online (or on reconnect)

				// add this device to my connections list
				// this value could contain info about the device or a timestamp too
				var con = myConnectionsRef.push(true)//.catch(Magneto.handleException);

				// when I disconnect, remove this device
				con.onDisconnect().remove()//.catch(Magneto.handleException);

				// when I disconnect, update the last time I was seen online
				lastOnlineRef.onDisconnect().set(firebase.database.ServerValue.TIMESTAMP).catch(Magneto.handleException);
			// }

			if (self._lastState && self._lastState !== 'state-offline') {
				self.setState('state-online'); // <--- restore last saved state if any
			}

			} else {

			// OFFLINE:

			if (self._lastState !== 'state-offline') {
				self._lastState = self.getState();
			}
			self.setState('state-offline');
			}
		})
		_isInstalled = true;
		// return _isInstalled;
	}

	self.listenToParentNode(observeInstanceRecordPresence, 'child_added')
	self.listenToParentNode(observeInstanceRecordPresence, 'child_removed')

	self.listenToAuthState(installPresenceMonitor)

}

module.exports = IOnline;