//switchable.js
var Magneto = require('./magneto');

var thisModName = 'IStateful';


// Import: (nothing)
var _extend = Magneto.extend;

// Export:
Magneto[thisModName] = IStateful;

/*
Has state
	*/

	// Private vars and functions:

// Для хранения стейта можем обойтись без отдельной переменной - пишем напрямую в атрибут класса:
var setAppState = function (domId, stateCode) {
	document.getElementById(domId).className = stateCode;
}

// И читаем оттуда же.
var getAppState = function (domId) {
	return document.getElementById(domId).className
}

function IStateful(self, data) {
	data = data || {}

	_extend(self, {
		'domId': data.domId || self.domId || 'i-stateful',
		'POSSIBLE_STATES': _extend(self.POSSIBLE_STATES || {}, data.POSSIBLE_STATES || {}),
		'_subscribers': self._subscribers || {}, // <-- listens to specific states notifications about changes
		/* ^ Do not loss previous subscribers if interface applied once more*/
		'_dispatchers': self._dispatchers || []
		/* ^ Do not loss previous dispatchers if interface applied once more*/

	})

	// Do not apply interface twice! but allow to extend states:
	if (self[thisModName]) return;
	self[thisModName] = true;

	_extend(self, {
		/**
		 * Apply state to device
		 * @param {string} stateCode
		 */
		'setState': function (stateCode) {
			if (self.POSSIBLE_STATES[stateCode] === void 0)
				throw new Error('Invalid state: '+stateCode);
			setAppState(self.domId, stateCode)

			// run dispatchers:
			for (var i=0, len=self._dispatchers.length; i<len; i++) {
				self._dispatchers[i](stateCode)
			}
			// run named subscribers
			var subsForState = self._subscribers[stateCode];
			if (!!subsForState) {
				for (var i=0, len=subsForState.length; i<len; i++) {
					subsForState[i]()
				}
			}
		},

		'getState': function () {
			return getAppState(self.domId)
		},

		'pushState': function () {
			self.history.push(self.getState())
		},

		'popState': function () {
			var state = self.history.pop();
			if (state) {
				self.setState(state)
			} else self.setState('state-stack-error')
		},

		/**
		 * Register handler to listen for a specific state. Handler will be called immediatelly when state changed.
		 * @param  {any} stateCode State constant
		 * @param  {function} handler  Callback (no aruments)
		 */
		'on': function (stateCode, handler) {
			if (!self._subscribers[stateCode]) {
				self._subscribers[stateCode] = []
			}
			if (typeof handler === 'function') {
				self._subscribers[stateCode].push(handler)
			} else Magneto.raise('"handler" argument for ".on" method must be a function')
		},

		/**
		 * Register handler to any state change. Handler will be called immediatelly when state changed.
		 * @param  {function} handler Callback with argument 'stateCode'
		 */
		'onStateChange': function (handler) {
			if (typeof handler === 'function') {
				self._dispatchers.push(handler)
			} else Magneto.raise('"handler" argument for ".onStateChange" method must be a function')
		}
	})

	// For UI - attach handlers to all elements with attribute "data-action-set-state=..."
	// Select all elements with attribute (more: https://developer.mozilla.org/en/docs/Web/CSS/Attribute_selectors)

	var jumpers = document.querySelectorAll('*[data-action-set-state]');

	[].forEach.call(jumpers, function (ctrlEl) {
		var stateId = ctrlEl.getAttribute('data-action-set-state');
		ctrlEl.addEventListener('click', function () {
			setTimeout(function () {
				self.setState(stateId)
			}, 1)
		})
	})


}

module.exports = IStateful;