// persistent.js
/*
Interacts with its reflection in DB
 */

var firebase = require('firebase');
var Magneto = require('./magneto');
var IAuthentic = require('./authentic');

var thisModName = 'IPersistent';

var _extend = Magneto.extend;


function IPersistent(self, data) {
	data = data || {};

	// Implements interface "Authentic":
	IAuthentic(self, data);

	_extend(self, {
		'dataDirectory': 	data.dataDirectory || '',

		// 'auth': 			firebase.auth(), // <-- moved to Authentic
		'database': 		firebase.database(),
		'storage': 			firebase.storage()
	})

	// Do not apply interface twice!:
	if (self[thisModName]) return;
	self[thisModName] = true;

	_extend(self, {
		'getKey': function () {Magneto.raise('getKey not implemented!')},
		'instanceDataPath': function () {
			return [self.dataDirectory, self.getKey()].join('/')
		},

		'dataRef': function () {
			return self.database.ref(self.dataDirectory)
		},

		/**
		 * [description]
		 * @param  {Function} callback function(user)
		 * @return {[type]}            [description]
		 */
		'listenToReadyState': function (callback) {
			self.listenToAuthState(callback)
		},

		/**
		 * Listens to the containing collection node
		 * @param  {Function} callback    [description]
		 * @param  {[type]}   eventFilter [description]
		 * @return {[type]}               [description]
		 */
		'listenToParentNode': function (callback, eventFilter) {
			var targetCallback = callback,
				targetFilter = (eventFilter || 'value'); /* <- listen to value changes by default */

			console.warn('*listenToParentNode', self.dataDirectory);
			var registrationFunc = function (user) {
				if (user) {
					var targetRef = self.database
						.ref(self.dataDirectory);

					console.warn('<<< listenToParentNode', targetRef.toString())

					// Setup listener:
					targetRef.off(targetFilter, targetCallback)
					targetRef.on(targetFilter, targetCallback)
				}
			}
			self.listenToReadyState(registrationFunc)
		},

		/**
		 * Aquire deferred registration of a permanent listener to the child node (.on is used inside). Registration would be after the first successful connection.
		 * @param  {string}   nodeName    Name of child node (can be either a simple name like 'myNodeName' or a compound name with slashes: 'my/sub/node')
		 * @param  {Function} callback    Handler, arguments: snapshot object
		 * @param  {string}   eventFilter Can be one of possible events: 'value', 'child_added', 'child_changed', .. (se FB reference)
		 */
		'listenToChildNode': function (nodeName, callback, eventFilter) {
			var targetCallback = callback,
				targetFilter = (eventFilter || 'value'), /* <- listen to value changes by default */
				targetNode = nodeName;

			var registrationFunc = function (user) {
				if (user) {
					var targetRef = self.database
						.ref(self.instanceDataPath())
						.child(targetNode);

					console.warn('<<< listenToChildNode: ',targetRef.toString())
					// Setup listener:
					targetRef.off(targetFilter, targetCallback) // <-- attention: can remove perviously registered listeners!
					targetRef.on(targetFilter, targetCallback)
				}
			}
			self.listenToReadyState(registrationFunc)

		},

		'listenToOwnNode': function (callback, eventFilter) {
			var targetCallback = callback,
				targetFilter = (eventFilter || 'value'); /* <- listen to value changes by default */

			var registrationFunc = function (user) {
				if (user) {
					var targetRef = self.database
						.ref(self.instanceDataPath());

					console.warn('<<< listenToOwnNode', targetRef.toString())

					// Setup listener:
					targetRef.off(targetFilter, targetCallback) // <-- attention: can remove perviously registered listeners!
					targetRef.on(targetFilter, targetCallback)
				}
			}
			self.listenToReadyState(registrationFunc)

		},

		/**
		 * [description]
		 * @param  {string} nodeName Name of child node (can be either a simple name like 'myNodeName' or a compound name with slashes: 'my/sub/node')
		 * @param  {any} value    Value to store
		 * @return {Promise}          Promise for chaining
		 */
		'saveToChildNode': function (nodeName, value) {
			var targetRef = self.database.ref(self.instanceDataPath());

			console.warn('>>> saveToChildNode', targetRef.child(nodeName).toString(), value)

			return targetRef
				.child(nodeName)
				.set(value)
		}

	})

}

module.exports = IPersistent;