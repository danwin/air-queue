//queue-client.js
var Magneto = require('./magneto');
var IPersistent = require('./persistent');

var thisModName = 'IQueueClient';

// Export:
Magneto[thisModName] = IQueueClient;
Magneto.constants['ERR_USER_ALREADY_CALLED'] = ++Magneto.constants.lastValue

var _extend = Magneto.extend;

var UUID = Magneto.UUID;

function IQueueClient(self, data) {
	data = data || {}

	// Setting for Properties:
	data.dataDirectory = data.dataDirectory || 'queue';
	// data.storageKeyForDeviceId = data.storageKeyForDeviceId || 'ys-device-id';
	// data.POSSIBLE_STATES = POSSIBLE_STATES,

	IPersistent(self, data)

	// New properties:
	_extend(self, {
		// 'credentials': data.credentials || {
		// 	'email': 'reception@qms.app',
		// 	'password': 'passwd'
		// },
		'queueRef': self.database.ref(self.dataDirectory),
		'ticketsRef': self.database.ref(self.dataDirectory).child('tickets'),
		'logRef': self.database.ref(self.dataDirectory).child('log')
	})

	// New methods:
	_extend(self, {

		'logAction': function (ticketId, state, ticketData, timestamp) {
			ticketData = ticketData || null;
			timestamp = timestamp || +(new Date());
			self.logRef.child('tickets').child(timestamp).set({
				'state': state,
				'ticketId': ticketId,
				'ticket': ticketData,

				'clientId': self.getKey() /* IPnP.getKey() used to identify client */
			})
			return true;
		},

		'createTicket': function (ticketArgs) {
			var CURRENT_STATE = 'state-new';
			var timestamp = +(new Date());
			/* Id, unique in the the current queue */
			var ticketId = ticketArgs.ticketId || UUID();
			var ticket = {
				/* Id, unique in the the current queue */
				'ticketDisplayText' : ticketArgs.ticketDisplayText,
				'ticketId': ticketId,
				'createdAt': timestamp,
				'basket' : ticketArgs.basket || {},
				'state' : CURRENT_STATE,
				'metadata' : ticketArgs.metadata
			}
			return self.ticketsRef.child(ticketId).set(ticket)
				.then(function () {
					self.logAction (ticketId, CURRENT_STATE, ticketArgs, timestamp)
					return ticket;
				})
		},

		'callTicket': function (ticketId) {
			var CURRENT_STATE = 'state-calling';
			var transactCode = function (currentState) {
				if (currentState !== CURRENT_STATE) {
					return CURRENT_STATE;
				} else { // ticket already called by somebody else!
					console.warn('Ticket already called!');
					return void(0); // Abort the transaction.
				}
			};
			var applyLocally = false;

			return self.ticketsRef
				.child(ticketId)
				.child('state')
				.transaction(transactCode, void(0), applyLocally)
					.then(function (result) {
						if (result.committed) {
							return Promise.resolve(true)
						} else {
							return Promise.reject(ERR_USER_ALREADY_CALLED)
						}
					})
						.then(function () {
							self.logAction (ticketId, CURRENT_STATE)
						})

		},
		// 'state-customer-session'

		'serveTicket': function (ticketId) {
			var CURRENT_STATE = 'state-customer-session';
			return self.ticketsRef
				.child(ticketId)
				.child('state')
				.set(CURRENT_STATE)
					.then(function () {
						self.logAction (ticketId, CURRENT_STATE)
					})
		},

		'closeTicket': function (ticketId) {
			var CURRENT_STATE = 'state-done';
			return self.ticketsRef
				.child(ticketId)
				// remove "order" from the basket, when empty - remove ticket... ?
				.remove()
					.then(function () {
						self.logAction (ticketId, CURRENT_STATE)
					})
		},

		'deferTicket': function (ticketId) {
			var CURRENT_STATE = 'state-deferred';
			return self.ticketsRef
				.child(ticketId)
				.child('state')
				.set(CURRENT_STATE)
					.then(function () {
						self.logAction (ticketId, CURRENT_STATE)
					})
		},

		// 'redirectTicket': function (ticketId, ... ) {
		// 	var CURRENT_STATE = 'state-redirected';
		// 	return self.queueRef
		// 		.child(ticketId)
		// 		.child('state')
		// 		.set(CURRENT_STATE)
		// 			.then(function () {
		// 				self.logAction (ticketId, CURRENT_STATE)
		// 			})
		// },

		'listenToQueue': function (callback, eventFilter) {
			self.ticketsRef.off(eventFilter, callback)
			self.ticketsRef.on(eventFilter, callback)
			console.warn('<<< listenToQueue', self.ticketsRef.toString())
		}
		// duality: redirect: destinationId, clientId

	})

}

module.exports = IQueueClient;