var firebase = require('firebase');
var Magneto = require('./magneto');

var thisModName = 'IAuthentic';


// Export:
Magneto[thisModName] = IAuthentic;
Magneto.constants['ERR_AUTH_TIMEOUT'] = ++Magneto.constants.lastValue

var _extend = Magneto.extend;

//authentic.js
function IAuthentic(self, data) {
	data = data || {};

	// Do not apply interface twice!:
	if (self[thisModName]) return;
	self[thisModName] = true;

	_extend(self, {
		'auth': firebase.auth()
	})

	_extend(self, {
		/**
		 * Sign In
		 * @return {Promise} Promise, with result Firebase.User
		 */
		'signIn': function (credentials) {
			return self.auth.signInWithEmailAndPassword(credentials.email, credentials.password)
		},
		'signOut': function () {
			return self.auth.signOut()
		},
		/**
		 * Add listener to the onAuthStateChanged
		 * @param  {Function} callback Handler with argument User
		 */
		'listenToAuthState': function (callback) {
			self.auth.onAuthStateChanged(callback)
		},

		/**
		 * Wait until first connection
		 * @return {[type]} [description]
		 */
		'waitConnection': function () {
			return new Promise(function (resolve, reject) {
				var cancelOnTimeout = function () {
					reject(Magneto.constants.ERR_AUTH_TIMEOUT)
				}
				var watchdog = setTimeout(cancelOnTimeout, 15000)
				self.listenToAuthState(function (user) {
					if (user) {
						clearTimeout(watchdog);
						resolve(user);
					} // <--- resolve outer promise
				})
			})
		},

		'retrieveSession': function () {
			// If already authenticated:
			if (self.auth.currentUser) Promise.resolve(self.auth.currentUser)
			return self.waitConnection()
		}
	})

}


module.exports = IAuthentic;