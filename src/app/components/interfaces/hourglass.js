// IHourglass
/**
 * TO-DO: use state + reflection instead
 */
var Magneto = require('./magneto');

var thisModName = 'IHourglass';

// Export:
Magneto[thisModName] = IHourglass;

var _extend = Magneto.extend;


function IHourglass(self, data) {
	data = data || {};
	_extend(self, {
		'houglassSelector': data.houglassSelector || 'hourglass'
	});

	var _lockCount = 0;

	_extend(self, {
		'beginWait': function () {
			if (!_lockCount) {
				var els = document.querySelectorAll(self.houglassSelector);
				[].forEach.call(els, function (el) {
					el.style.display = ''
				})
			}
			_lockCount++;
		},
		'endWait': function () {
			_lockCount--;
			if (!_lockCount) {
				var els = document.querySelectorAll(self.houglassSelector);
				[].forEach.call(els, function (el) {
					el.style.display = 'none'
				})
			}
		}
	});
}

module.exports = IHourglass;
