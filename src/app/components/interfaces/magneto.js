//Magneto


var Magneto = {
	raise: function (message) {throw new Error(message)}
};

var Promise = window.Promise || Magneto.raise('Magneto requores Promise API. Update browser or use polyfill.')

/**
 * Namespace for constants.
 * 'lastValue' simplifies creation of new constants with unique values:
 * var NEW_CONSTANT = ++Magneto.constants.lastValue
 * @type {Object}
 */
Magneto.constants = {'lastValue': 0}

// Magneto.implement = function (interfaceName, self, data) {
// 	// _isA - a set with flags which allow to prevent application of interface if it already applied.
// 	if (!self._isA) {
// 		self.isA = {};
// 	} else if (self._isA[interfaceName]) {
// 		// Already implements this interface, do not apply more then 1 time, so exiting:
// 		return;
// 	}
// 	var factory = Magneto[interfaceName];
// 	if (factory === void 0)
// 		Magneto.raise ('Requires interface "'+interfaceName+'"!'); // js module missed!
// 	// Data is optional: assign empty object if undefined:
// 	data = data || {};
// 	factory(self, data)
// 	// Mark that the object has interface:
// 	self._isA[interfaceName] = true;
// }

Magneto.extend = function (destObj, srcObj) {
	for (var name in srcObj) {
		if (srcObj.hasOwnProperty(name)) destObj[name] = srcObj[name]
	}
	return destObj
}

Magneto.handleException = function (e) {
	console.error('Mg error: ', e)
}

Magneto.wait = function (delayMs) {
	return new Promise(function (resolve, reject) {
		setTimeout(resolve, delayMs)
	})
}

Magneto.forEachElement = function (selector, iterator) {
	var elements = document.querySelectorAll(selector);
	[].forEach.call(elements, iterator)
}

Magneto.UUID = function () {
	// Implementation suggested: http://stackoverflow.com/a/2117523
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		return v.toString(16);
	}).toUpperCase();
}

Magneto.assertDefined = function (value, errmessage) {
	if (value === void(0)) throw new Error(errmessage)
	return value
}


module.exports = Magneto;