//onscreenmenu.js

var Magneto = require('./magneto');

var thisModName = 'IMenu';

var _extend = Magneto.extend;

// Export:
Magneto[thisModName] = IMenu;

function IMenu(self, data) {
	data = data || {}

	_extend(self, {
		'menuDomId': data.menuDomId || 'menu'
	})

	// Do not apply interface twice!:
	if (self[thisModName]) return;
	self[thisModName] = true;

	var selectedItem = null;
	var savedState = null;

	// Methods:
	_extend(self, {
		'showMenu': function () {
			document.getElementById(self.menuDomId).className = 'show-menu'
		},
		'hideMenu': function () {
			document.getElementById(self.menuDomId).className = ''
		}
	})


	var handleKeyUp = function (evt) {
		var e = evt ? evt : window.event;
		var key = e.charCode || e.keyCode || e.which;
		// was: e.keyCode
		if (17 === key) {
			self._isCtrlKeyDown = false;
		}
		if (18 === key) {
			self._isAltKeyDown = false;
		}
	}

	var handleKeyDown = function (evt) {
		var e = evt ? evt : window.event;
		var key = e.charCode || e.keyCode || e.which;
		var keychar = String.fromCharCode(key);

		// // ----------- 8< --------------------
		// // Simple stub to allow only Tab key
		// if (key === 9) return true;
		// return _stopEvent(e)
		// // ----------- 8< --------------------

		// Ctrl key:
		if (key === 17) {
			self._isCtrlKeyDown = true;
			return true;
		}
		// Alt key:
		if (key === 18) {
			self._isAltKeyDown = true;
			return true;
		}

		if (self._isCtrlKeyDown && self._isAltKeyDown && key == 77) {
			// Ctrl + Alt + M
			self.showMenu()
		} else if (key == 27) {
			// Escape
			self.hideMenu();
		}
	}

	// listen to keyboard
	var eventHub = document.getElementsByTagName('body')[0];
	eventHub.addEventListener('keyup', handleKeyUp)
	eventHub.addEventListener('keydown', handleKeyDown)

}

module.exports = IMenu;