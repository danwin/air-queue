var Magneto = require('./../interfaces/magneto');

var IPersistent = require('./../interfaces/persistent');

var _ = require('underscore');

var thisModName = 'UserRegistry';

// Export:
Magneto[thisModName] = UserRegistry;
Magneto.constants['ERR_USER_ALREADY_CALLED'] = ++Magneto.constants.lastValue

var _extend = Magneto.extend;
var assertDefined = Magneto.assertDefined;

var UUID = Magneto.UUID;

function UserRegistry(data) {
	data = data || {}
	var self = {}

	// Setting for Properties:
	data.dataDirectory = data.dataDirectory || 'user-registry';
	// data.storageKeyForDeviceId = data.storageKeyForDeviceId || 'ys-device-id';
	// data.POSSIBLE_STATES = POSSIBLE_STATES,

	IPersistent(self, data)

	// New properties:
	_extend(self, {
	})

	var getUsersRef = function () {
		console.log('---> getRegistryRef', self.database.ref(self.dataDirectory).child('users').toString())
		return self.database.ref(self.dataDirectory).child('users')
	}

	var getLogRef = function () {
		console.log('---> getLogRef', self.database.ref(self.dataDirectory).child('log').toString())
		return self.database.ref(self.dataDirectory).child('log')
	}


	var getValueFromSnapshot = function (snapshot) {
		return snapshot.val()
	}


	// New methods:
	_extend(self, {

		// 'logAction': function (ticketId, state, ticketData, timestamp) {
		// 	ticketData = ticketData || null;
		// 	timestamp = timestamp || +(new Date());
		// 	getLogRef().child('tickets').child(timestamp).set({
		// 		'state': state,
		// 		'ticketId': ticketId,
		// 		'ticket': ticketData,

		// 		'clientId': self.getKey() /* IPnP.getKey() used to identify client */
		// 	})
		// 	return true;
		// },

		'findUserByUUID': function (uuid) {
			var criteria = uuid
			var find = function (obj) {
				var result = _.findKey(obj, function (value) {
					return (value.authUUID === criteria)
				})
				if (result) return result
				return Promise.reject('Not found')
			}
			return getUsersRef()
				.once(value)
				.then(find)
		}

		'createUser': function (userInfo) {
			var CURRENT_STATE = 'state-new'; // also: state-valid, state-locked
			var timestamp = +(new Date());
			var raiseIfExists = function (result) {
				if (result.val() !== null) return Promise.reject('User "{user}"" already exists!'.replace(/user/g, userInfo.email))
				return result
			}
			var injectData = function (data) {
				var value = data;
				return function () {
					return data
				}
			}
			var saveUser = function (userInfo) {
				return getUsersRef()
					.child(userInfo.email)
					.set(userInfo)
			}
			return getUsersRef()
				.child(userInfo.email)
				.once('value')
				.then(raiseIfExists)
				.injectData(userInfo)
				.then(saveUser)
		},

		'lockUser': function (uuid) {
			var changeLock = function (userInfo) {
				return getUsersRef()
					.child(userInfo.email)
					.child('isLocked')
					.set(true)
			}
			return findUserByUUID(uuid)
				.then(updateState)
		},

		'unlockUser': function (uuid) {
			var changeLock = function (userInfo) {
				return getUsersRef()
					.child(userInfo.email)
					.child('isLocked')
					.set(false)
			}
			return findUserByUUID(uuid)
				.then(updateState)
		},


		'removeUser': function (uuid) {
			var deleteRecord = function (userInfo) {
				return getUsersRef()
					.child(userInfo.email)
					.remove()
			}
			return findUserByUUID(uuid)
				.then(deleteRecord)
					.then(function () {
						self.logAction (uuid, 'removed')
					})
		}


		'listenToRegistry': function (callback, eventFilter) {
			getUsersRef().off(eventFilter, callback)
			getUsersRef().on(eventFilter, callback)
			console.warn('<<< listenToRegistry', self.ticketsRef.toString())
		}

	})

	return self

}

module.exports = UserRegistry;