// user.js
// persistent.js
/*
Interacts with its reflection in DB
 */

var Magneto = require('./../interfaces/magneto');

var IPersistent = require('./../interfaces/persistent');

// var IStateful = require('./../interfaces/stateful');
// var IPingable = require('./../interfaces/pingable');
// var IOnline = require('./../interfaces/online');

// var IPnP = require('./../interfaces/pnp');
// // var IMenu = require('./../interfaces/menu');

// var IQueueClient = require('./../interfaces/queue-client');

// var IHourglass = require('./../interfaces/hourglass');
// var DisplayClient = require('./../echo-glass/display-client');
// // var PnPManager = require('./pnp-manager');

var dialogWindow = require('./../utils/dialog.window');

var _ = require('underscore');

var thisModName = 'User';

// Export:
Magneto[thisModName] = User;

var _extend = Magneto.extend;
var assertDefined = Magneto.assertDefined;

User.POSSIBLE_STATES = {
	'state-new': true,
	'state-valid': true,
	'state-locked': true
};

function User(data) {
	data = data || {};
	var self = {}

	_extend(self, {
		'accountEmail': data.accountEmail,

		'firstName' : data.firstName || '',
		'lastName':  data.lastName || '',

		'state': data.state || 'state-new',

		'isAdmin': data.isAdmin || false,


		/* Photo field */
		'photoUrl': data.photoUrl || '',

		'dataDirectory': 	data.dataDirectory || 'user-registry/users'
	})

	// Do not apply interface twice!:
	if (self[thisModName]) return;
	self[thisModName] = true;

	// Implements interface "Authentic":
	IPersistent(self, data);

	var getUsersRef = function () {
		console.log('---> getRegistryRef', self.database.ref(self.dataDirectory).child('users').toString())
		return self.database.ref(self.dataDirectory).child('users')
	}

	var getLogRef = function () {
		console.log('---> getLogRef', self.database.ref(self.dataDirectory).child('log').toString())
		return self.database.ref(self.dataDirectory).child('log')
	}

	var instanceRef = function (deviceId) {
		console.log('User ref:', self.database
			.ref(self.dataDirectory)
			.child(self.getKey()).toString())
		return self.database
			.ref(self.dataDirectory)
			.child(self.getKey())
	}

	/*
	Utilities for promise chain:
		*/
	var chain = {}; // Namespace to differ promise callbacks
	chain.raiseWithAuthErrorMessage = function (error) {
		var messages = {
			'auth/email-already-in-use':
				'Sorry, you cannot use this e-mail address here. Contact administrator, please',
			'auth/invalid-email':
				'This e-mail address is invalid',
			'auth/operation-not-allowed':
				'Email-based accounts disabled. Contact administrator, please',
			'auth/weak-password':
				'This password is weak. Use at least 6 symbols.'
		};
		var message = (error && error.code) ? messages[error.code] : 'Cannot activate account. Unknown error.';
		return Promise.reject(message)
	}

	chain.getValueFromSnapshot = function (snapshot) {
		return snapshot.val()
	}
	chain.raiseWithMessage = function (message) {
		var text = message;
		return function (value) {
			return Promise.reject(text)
		}
	}
	chain.reaiseOnNull = function (message) {
		var text = message;
		return function (value) {
			if (value === null) return Promise.reject(text)
			return value
		}
	}
	chain.extendResponseData = function (newData) {
		var e = newData
		return function (response) {
			return _extend(response, e)
		}
	}
	chain.passResult = function (newValue) {
		var e = newValue
		return function (response) {
			return newValue
		}
	}

	_extend(self, {

		'getKey': function () {return self.accountEmail},

		'isLocked': function () {
			return (self.state === 'state-locked')
		},

		'save': function () {
			return instanceRef().update({
				'authUUID' : data.authUUID
				'firstName' : data.firstName,
				'lastName':  data.lastName,

				'state': data.state,

				'isLocked': data.isLocked,
				'isAdmin': data.isAdmin,

				/* Photo field */
				'photoUrl': data.photoUrl,

			})
		},

		'load': function () {
			return instanceRef().once(function (snapshot) {
				var value = snapshot.val();
				_extend(self, value)
			})
		},

		'uploadPhoto': function (fileObj) {
			Magneto.raise('Not Implemented!')
		},
		/*
		Methods for management
			*/
		'signUp': function () {
			var data = {
				'email': '',
				'password': '',
				'passwordConfirmation': ''
			};
			// Promise: step 0
			var filterDialogResult = function (Ok) {

				if (!Ok) throw new Error('Cancelled by user');

				// extract data:
				data.email = document.querySelector('input[data-dialog-value="email"]').value
				data.password = document.querySelector('input[data-dialog-value="password"]').value
				data.password = document.querySelector('input[data-dialog-value="passwordConfirmation"]').value

				if (data.password !== data.passwordConfirmation) {
					// console.error(data.password, data.passwordConfirmation)
					throw new Error('Password and password confirmation are different!');
				}
				return data
			}
			// Promise: step 1
			var createProviderAccount = function (credentials) {
				var email = assertDefined(credentials.email, 'email required!');
				var password = assertDefined(credentials.password, 'password required!');

				return self.auth.createUserWithEmailAndPassword(email, password)  // <--- resolving with Firebase.User
					.catch(chain.raiseWithAuthErrorMessage)
			}

			// Promise: step 2
			var checkPreRegistration = function (rawUser) {
				// Assumes that new user is already signed (or registered in DB) in a system
				if (rawUser.email === self.auth.currentUser.email) {
					return getUsersRef().child(rawUser.email)
						.once('value')
						.then(chain.getValueFromSnapshot)
						.then(chain.reaiseOnNull('Sorry, your account was not confirmed by Administrator.'))
						.then(chain.extendResponseData({'authUUID': rawUser.uid}))
				} else {
					return Promise.reject('Cannot sign in the New user!')
				}
			}

			// Promise: step 3
			var store3rdPartyUID = function (extendedUserRec) {
				console.log('~extendedUserRec', extendedUserRec)
				return getUsersRef()
					.child(extendedUserRec.email)
					.child('authUUID')
					.set(extendedUserRec.authUUID)
			}

			var greetings = function () {
				return window.showAlert('Welcome!', 'Congratulations', 'info');
			}

		var deleteProviderAccount = function (reason) {
			return new Promise(function (resolve, reject) {
				// User not found in extended DB:
				if (app.auth.user && app.auth.user.email === data.email) {
					app.auth.deleteCurrentUser()
						.then(function (response) {
							window.showAlert('Sorry, your account was not confirmed by Administrator.');
							console.log('temporary user successfully removed!', response)
							resolve()
						})
						.catch(function (reason) {
							console.error('Error removing temporary user!', reason)
							reject('Error removing temporary account')
						})
				} else reject(reason)
			})
		}

// ----
			var DialogSignUp = dialogWindow.fromTemplate('dialog-assign-display')
			DialogSignUp.show(data)
				// arg: Ok
				.then(filterDialogResult)
				// arg: {email, password}
				.then(createProviderAccount)
				// arg: rawUser
				.then(checkPreRegistration)
				// arg: preRegisteredData
				.then(store3rdPartyUID)
				// arg: none
				.then(greetings)


				// arg: reason, internally uses "data" and "auth.user"
				.catch(deleteProviderAccount)
				// if something happened on deleteProviderAccount
				.catch(Magneto.handleException)



			}
	})

	return self;

}

module.exports = User;