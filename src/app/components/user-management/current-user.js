// current-user.js
// user.js
// persistent.js
/*
Interacts with its reflection in DB
 */

var Magneto = require('./../interfaces/magneto');

var IStateful = require('./../interfaces/stateful');

var User = require('./user');

var thisModName = 'CurrentUser';

// Export:
Magneto[thisModName] = CurrentUser;

var _extend = Magneto.extend;
var assertDefined = Magneto.assertDefined;

CurrentUser.POSSIBLE_STATES = {
	'state-new': true,
	'state-valid': true,
	'state-locked': true
};

var NO_USER = {
	'accountEmail': '',
	'firstName' : '',
	'lastName':  '',
	'state': 'state-locked',
	'isAdmin': false,
	/* Photo field */
	'photoUrl': '',
}

function CurrentUser(data) {
	data = data || {};
	var self = User(data); // <--- implements IPersistent also

	_ctxCallbacks = [];

	// Implements interface "Authentic":
	IStateful(self, {
		'domId': 'user-info', /* Not same as the app state, so use another node in DOM*/
		'POSSIBLE_STATES': CurrentUser.POSSIBLE_STATES
	})

	// Do not apply interface twice!:
	if (self[thisModName]) return;
	self[thisModName] = true;

	var ownRef = function () {
		return self.database(self.dataDirectory).child(self.getKey())
	}

	_extend(self, {
		'onUserCtxChanged': function (callback) {
			_ctxCallbacks.push(callback)
		}
	});

	var reflectCurrentUser = function (user) {
		if (user) {
			var targetRef = ownRef();
			self.accountEmail = user.email;
			targetRef.off()
			targetRef.on('value', function (snapshot) {
				var value = snapshot.val();
				_extend(self, value) // read data from snapshot
				_ctxCallbacks.forEach(function (callback) {
					callback(self)
				});
			})
		} else {
			_extend(self, NO_USER)
		}
	}

	self.listenToAuthState(reflectCurrentUser)

	return self;

}

module.exports = CurrentUser;