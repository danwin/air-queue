
var Magneto = require('./../interfaces/magneto');

var IPersistent = require('./../interfaces/persistent');

var IStateful = require('./../interfaces/stateful');
var IPingable = require('./../interfaces/pingable');
var IOnline = require('./../interfaces/online');

var IPnP = require('./../interfaces/pnp');
// var IMenu = require('./../interfaces/menu');

var IQueueClient = require('./../interfaces/queue-client');

var IHourglass = require('./../interfaces/hourglass');
var DisplayClient = require('./../echo-glass/display-client');
// var PnPManager = require('./pnp-manager');

// var dialogWindow = require('./../utils/dialog.window');

var _ = require('underscore');

var thisModName = 'OperatorNode';

var _extend = Magneto.extend;
var wait = Magneto.wait;
var forEachElement = Magneto.forEachElement;

// Export:
Magneto[thisModName] = OperatorNode;


// Перечисляем "допустимые" состояния для терминала-регистратора.
var POSSIBLE_STATES = {
	'state-trace': true, /* New device - no registration record in database*/
	'state-idle': true, /* */

	'state-selection': true,
	'state-enter-person-id': true,
	'state-display-ticket-info': true,

	'state-ready': true,
	'state-calling': true,
	'state-customer-session': true,
	'state-session-done': true,
	// 'state-': true,

	'state-maintenance': true,
	'state-error': true,
	'state-signed-off': true, /* Authentication failure */
	'state-offline': true /* Network problem - no connection */
	}

var SERVICE_LABELS = {
	'deposit': 'Deposit operations',
	'loan-application': 'Loan application',
	'cash-out': 'Cash out',
	'consult': 'Consult'
}


function OperatorNode(data) {
	data = data || {}
	// Properties
	var self = {}

	// Setting for Properties:
	data.deviceType = 'device-operator-console';
	data.busDirectory = 'PnPBus';
	data.dataDirectory = data.dataDirectory || 'operator-nodes';
	data.storageKeyForDeviceId = data.storageKeyForDeviceId || 'device-id';
	data.POSSIBLE_STATES = POSSIBLE_STATES;

	self.credentials = {
		'email': 'manager@qms.app',
		'password': 'passwd'
	}

	IPersistent(self, data)
	IStateful(self, data)
	IPnP(self, data)
	IOnline(self, data)
	IHourglass(self, {'houglassSelector': '.ajax-wait'})

	// Note IQueueClient has another dataDirectory so make a separate object for it (do not apply this interface to "self" directly)!
	self.qClient = {};
	var qcData = _extend({}, data);
	qcData.dataDirectory = 'ticket-devices';
	IQueueClient(self.qClient, qcData)

	self.qClient.getKey = self.getKey; // use the same key function for queue client

	self.dClient = DisplayClient({
		'deviceType': 'device-display',
		'dataDirectory': 'displays',
	})

	// New properties:
	// New properties:
	_extend(self, {
		'queueBuffer': [], /* Current state of queue */
		'selectedTicket': null, /* Selected (but not miodified) ticket */
		'activeTicket': null, /* Ticket which is under changes */
		'handleException': function (error) {
			self.endWait();
			console.error('handleException: ', error)
		}
	});

	var _2digits = function (value) {
		return (value < 10) ? '0'+value : value;
	}

	var fmtTime = function (timeObj) {
		var hours = timeObj.getHours(),
			minutes = timeObj.getMinutes(),
			seconds = timeObj.getSeconds();
		return [
				_2digits(hours),
				_2digits(minutes),
				_2digits(seconds)
			].join(':');
	}

	function fmtBasket(basket) {
		var buffer = [];
		for (var name in basket) {
			if (basket.hasOwnProperty(name)) buffer.push(SERVICE_LABELS[name])
		}
		return buffer.join(',')
	}

	// Helper to inject state switch into promises:
	var setNextState = function (stateId) {
		var sId = stateId;
		return function (result) {
			self.setState(sId)
			return result
		}
	}

	var clearActiveTicket = function () {
		self.activeTicket = null;
	}

	var displayQueueLength = function () {
		var len = self.queueBuffer.length;
		var placeholders = document.querySelectorAll('*[data-display-value="queue-length"]');
		[].forEach.call(placeholders, function (el) {
			if (len > 0) {
				// show badge if queue is non-empty and display length:
				el.innerHTML = len
				el.style.display = ''
			} else {
				// hide badge if queue is empty:
				el.style.display = 'none'
			}
		})
	}

	var displayStatusMessage = function (message) {
		var placeholders = document.querySelectorAll('*[data-display-value="status-message"]');
		[].forEach.call(placeholders, function (el) {
			el.innerHTML = message
		})
	}

	var updateSelectionInfo = function () {
		var ticket = self.selectedTicket;
		var ticketInspector = document.querySelector('*[data-display-value="ticket-inspector"]')
		if (ticket && ticketInspector) {
			var interior =
				/*Load inner HTML as template, replace alues and copy to destination:*/
				document.getElementById('queue-inspector').innerHTML
				.replace(/{ticketId}/g, ticket.ticketId)
				.replace(/{ticketDisplayText}/g, ticket.ticketDisplayText)
				.replace(/{timestamp}/g, fmtTime(new Date(ticket.createdAt)))
				.replace(/{info}/g, fmtBasket(ticket.basket))
			ticketInspector.innerHTML = interior
		} else {
			ticketInspector.innerHTML = interior
		}
	}

	/**
	 * [runTimer description]
	 * @param  {[type]} targetSelector [description]
	 * @param  {[type]} stopAtSecond   [description]
	 * @return {[type]}                [description]
	 */
	var runTimer = function (targetSelector, stopAtSecond) {
		return new Promise(function (resolve, reject) {
			var elapsed = 0; // How many seconds elapsed
			var intervalMs = 1000; // <-- 1 sec
			var lastSecond = stopAtSecond || 60; // <-- kill timer after reaching this value;
			var selector = targetSelector;

			function iterateSecond () {
				var now = new Date();
				var time = new Date(0,0,0,0,0, elapsed); // <-- use only "seconds" argument

				[].forEach.call(document.querySelectorAll(selector), function (el) {
					el.innerHTML = fmtTime(time)
				});
				elapsed++; // <-- next value for seconds counter;
				if (elapsed < lastSecond) {
					// Call "iterateSecond" recursively, with time compensation:
					setTimeout(iterateSecond, intervalMs - (now % intervalMs))
				} else {
					// Resolve promise after timeout!:
					resolve(elapsed)
				}
			}

			// Start chain:
			iterateSecond()
		})
	}

	// var DialogCalling = window.dialogWindow.fromTemplate('dialog-calling')

	// Translate PnP states:
	self.on('state-pnp-done', function () {
		document.title = document.title.replace(/\[.*\]/g, '['+(self.deviceAddr || '-') +']')
		wait(1000)
			.then(setNextState('state-ready'))
	})

	self.on('state-online', function () {
		self.setState('state-ready')
	})

	// self.on('state-online', function () {self.setState('state-ready')})

	var setHandler = function (prefix) {
		// Returns function to call in promise
		return function (reason) {
			console.error(prefix, reason)
		}
	}


	// Handle state:
	self.on('state-calling', function () {
		if (self.dClient.displayId) self.dClient.send(
			self.dClient.displayId,
			self.activeTicket.ticketDisplayText,
			'state-calling',
			false)
			.catch(setHandler('state-calling, error setting display output'));
		displayStatusMessage('Calling ticket: '+ self.activeTicket.ticketDisplayText);
		runTimer('*[data-display-value="waiting-time-elapsed"]', 5 * 60);
		// <-- 5 minutes max
		// DialogCalling.show()
		// 	.then(function (result) {
		// 		// setState(result)
		// 		console.log(result)
		// 	})
	})

	// Handle state:
	self.on('state-customer-session', function () {
		if (self.dClient.displayId) self.dClient.send(
			self.dClient.displayId,
			self.activeTicket.ticketDisplayText,
			'state-customer-session',
			false)
			.catch(setHandler('state-customer-session, error setting display output'));
		displayStatusMessage('Working with ticket: '+ self.activeTicket.ticketDisplayText);
		runTimer('*[data-display-value="service-time-elapsed"]', 5 * 60); // <-- 5 minutes max
	})

	// Handle state:
	self.on('state-session-done', function () {
		displayStatusMessage('Session finished: '+ self.activeTicket.ticketId);
		wait(3000)
			.then(setNextState('state-ready'))
	})

	self.addTagToBasket = function (serviceId) {
		self.basket[serviceId] = true
	}

	self.callTicket = function (qItem) {
		if (qItem) {
			var ticketId = qItem.ticketId;
			self.activeTicket = qItem;
			self.beginWait() // <-- IHourglass.beginWait()
			self.qClient.callTicket(ticketId)
				.then(setNextState('state-calling'))
				.catch(self.handleException)
				.then(self.endWait) // <-- here ".then" acts as a "finally"
		}
	}

	self.callTicketFirst = function () {
		self.callTicket(self.queueBuffer[0])
	}

	self.callTicketSelected = function () {
		self.callTicket(self.selectedTicket)
		self.selectedTicket = null;
	}

	self.serveTicket = function () {
		var ticketId = self.activeTicket.ticketId;
		self.beginWait() // <-- IHourglass.beginWait()
		self.qClient.serveTicket(ticketId)
			.then(setNextState('state-customer-session'))
			.catch(self.handleException)
			.then(self.endWait) // <-- here ".then" acts as a "finally"
	}

	self.closeTicket = function () {
		var ticketId = self.activeTicket.ticketId;
		self.beginWait() // <-- IHourglass.beginWait()
		self.qClient.closeTicket(ticketId)
			.then(setNextState('state-session-done'))
			.catch(self.handleException)
			.then(self.endWait) // <-- here ".then" acts as a "finally"
	}

	self.deferTicket = function () {
		var ticketId = self.activeTicket.ticketId;
		self.beginWait() // <-- IHourglass.beginWait()
		self.qClient.deferTicket(ticketId)
			.then(setNextState('state-ready'))
			.catch(self.handleException)
			.then(self.endWait) // <-- here ".then" acts as a "finally"
	}

	self.redirectTicket = function () {
		var ticketId = self.activeTicket.ticketId;
		self.beginWait() // <-- IHourglass.beginWait()
		self.qClient.deferTicket(ticketId)
			.then(setNextState('state-session-done'))
			.catch(self.handleException)
			.then(self.endWait) // <-- here ".then" acts as a "finally"
	}

	// Create listeners
	// Select all elements with attribute (more: https://developer.mozilla.org/en/docs/Web/CSS/Attribute_selectors)

	forEachElement('*[data-action-select-service]',
		function (ctrlEl) {
			var serviceId = ctrlEl.getAttribute('data-action-select-service');
			ctrlEl.addEventListener('click', function () {
				self.addTagToBasket(serviceId)
			})
		})

	forEachElement('*[data-action-code="call-first"]',
		function (ctrlEl) {
			ctrlEl.addEventListener('click', self.callTicketFirst)
		})

	forEachElement('*[data-action-code="call-selected"]',
		function (ctrlEl) {
			ctrlEl.addEventListener('click', self.callTicketSelected)
		})

	forEachElement('*[data-action-code="serve-ticket"]',
		function (ctrlEl) {
			ctrlEl.addEventListener('click', self.serveTicket)
		})

	forEachElement('*[data-action-code="close-ticket"]',
		function (ctrlEl) {
			ctrlEl.addEventListener('click', self.closeTicket)
		})

	forEachElement('*[data-action-code="defer-ticket"]',
		function (ctrlEl) {
			ctrlEl.addEventListener('click', self.deferTicket)
		})

	forEachElement('*[data-action-code="redirect-ticket"]',
		function (ctrlEl) {
			ctrlEl.addEventListener('click', self.redirectTicket)
		})

	forEachElement('*[data-action-set-state]',
		function (ctrlEl) {
			var stateId = ctrlEl.getAttribute('data-action-set-state');
			ctrlEl.addEventListener('click', function () {
				self.setState(stateId)
			})
		})

	forEachElement('*[data-action-register-ticket]',
		function (ctrlEl) {
			var setting = ctrlEl.getAttribute('data-action-register-ticket');
			if (setting.match(/^\input#/)) {
				var id = setting.replace(/^input#/, '');
				console.log('submitters: ', setting, id);
				var input = document.getElementById(id);
				if (input) {
					ctrlEl.addEventListener('click', function () {
						self.ticketDisplayText = input.value;
					})
				} else console.error('Cannot find element "#'+id+'" from "data-action-register-ticket" attribute!')
			} else console.error('invalid value of attribute "data-action-register-ticket": ', setting)
		})

	// selection for ticket from queue:
	document.getElementById('queue').addEventListener('click',
		function (event) {
			var e = event || window.event;
			var sourceEl = event.target.parentNode;
			var ticketId = sourceEl && sourceEl.getAttribute('data-ticket-id') || null;
			console.log(sourceEl, e.target);
			if (ticketId) {
				if (self.selectedTicket) {
					// deselect prevoius ticket if any:
					var element = document.querySelector('*[data-ticket-id="'+self.selectedTicket.ticketId+'"]');
					if (element) element.classList.remove('selected')
				}
				self.selectedTicket = _.find(self.queueBuffer, function (ticket) {
					return (ticketId === ticket.ticketId)
				}) || null;
				sourceEl.classList.add('selected')
				updateSelectionInfo();
			}
		}, true);


	var observeQueue = function (snapshot) {
		console.log(snapshot.val());
		var root = document.getElementById('queue');
		var queue = snapshot.val();
		var buffer = [];
		for (var id in queue) {
			if (queue.hasOwnProperty(id)) {
				var ticket = queue[id];
				// filter waiting tickets  with "state-new"
				if (ticket.state === 'state-new') {
					// Copy the ticket object and extend and add "ticketId" property:
					ticket.ticketId = id
					buffer.push(ticket)
				}
			}
		}
		// Sort array in descent order:
		buffer.sort(function (a, b) {
			return b.createdAt - a.createdAt
		})

		// Create DOM elements for array items
		var rows = [];

		var order = 0;
		buffer.forEach(function (ticket) {
			rows.push(
				document.getElementById('queue-row').innerHTML
					.replace(/{ticketId}/g, ticket.ticketId)
					.replace(/{order}/g, ++order)
					.replace(/{ticketDisplayText}/g, ticket.ticketDisplayText)
					.replace(/{timestamp}/g, fmtTime(new Date(ticket.createdAt)))
					.replace(/{info}/g, fmtBasket(ticket.basket))
			)
		})
		root.innerHTML = rows.join('');
		// Store a local copy:
		self.queueBuffer = buffer;

		// Update length indicator / badge:
		displayQueueLength()
	}

	self.on('state-ready', function () {
		if (self.dClient.displayId) self.dClient.send(
			self.dClient.displayId,
			'--:--',
			'state-ready',
			false)
			.catch(setHandler('state-ready, error setting display output'));

		// Install queue observer:
		console.log('-1-')
		self.qClient.listenToQueue(observeQueue, 'value');
		console.log('-2-')
		displayStatusMessage('Ready');
		self.endWait();
	})



	var autoSignIn = function () {
		return	self.signIn(self.credentials)
			// self.qClient(self.credentials)
	}

	var readSettings = function () {
		var targetRef = self.database
						.ref(self.instanceDataPath())
						.child('settings');
		return new Promise(function (resolve, reject) {
			return targetRef.once('value')
				.then( function (snapshot) {
					var settings = snapshot.val();
					var displayId = settings ? settings.displayId : null;
					self.dClient.displayId = displayId;
					if (!displayId) self.setState('state-error')
					displayStatusMessage('Display is not assigned!')
					// This callback will be also called later outside this context!:
					resolve(settings);
				})
				.catch(reject)
		})
	}

	var observeSettings = function () {
		self.listenToChildNode('settings', function (snapshot) {
			var settings = snapshot.val();
			var displayId = settings ? settings.displayId : null;
			self.dClient.displayId = displayId;
			if (!displayId) self.setState('state-error')
			displayStatusMessage('Display is not assigned!')
		}, 'value')
	}

	// retrieveDeviceId();

	self.beginWait()

	self.retrieveSession()
		.catch(autoSignIn)
		.then(readSettings)
		.then(function () {
			console.log('Magneto Operator node:  Signed!')
			// Enter to the start state:
			// self.setState('state-ready')
			// install settings observer:
			// observeSettings()
		})
		.catch(function (reason) {
			console.error('Auth error: ', reason)
			self.setState('state-signed-off')
		})

	console.log('operator node: ', self)

	return self;


}

module.exports = OperatorNode;