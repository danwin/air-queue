//display-manager.js


// Перечисляем "допустимые" состояния для табло. Обрати внимание - значения "совместимы" с именами классов CSS - начинаются не с цифры и т.п. Этот массив просто для проверки что мы присваиваем "законное" состояние, предусмотренной в системе (используя [...].indexOf(state) >-1 )
DisplayManager.POSSIBLE_STATES = [
	'state-trace', /* New device - no registration record in database*/
	'state-service-menu', /* Show on-screen menu*/
	'state-idle', /* */
	'state-ready',
	'state-calling', /*  */
	'state-customer-session', /*  */
	'state-session-done',
	'state-maintenance',
	'state-error',
	'state-signed-off', /* Authentication failure */
	'state-offline' /* Network problem - no connection */
	]

// Для хранения стейта можем обойтись без отдельной переменной - пишем напрямую в атрибут класса:
var setAppState = function (stateCode) {
	document.getElementsByTagName('body')[0].className = stateCode;
}

// И читаем оттуда же. Функция пока не используется, записана чтобы не забыть в будущем
var getAppState = function () {
	return document.getElementById('i-stateful').className
}

// content can be: text, html; ...
// field "lastOnline" - to clear old records (from another consoles - Manager, Admin?)

var extend = function (destObj, srcObj) {
	for (var name in srcObj) {
		if (srcObj.hasOwnProperty(name)) destObj[name] = srcObj[name]
	}
	return destObj
}

function DisplayManager(config) {
	// Properties
	var self = {
		'groupDataRef' : 	null, /* Reference to data with all devices*/
		'deviceCount': 		null,
		'auth': 			firebase.auth(),
		'database': 		firebase.database(),
		'storage': 			firebase.storage(),
		'onListChanged': 	null, /* Callback to notify about changes in device list*/
		'onConnected': 		null /* Callback notifies about the establised connection to DB data*/
	}
	// Methods
	self = extend(self, {
		'devicesDataPath': function () {
			return 'displays'
		},
		'announcePath': function () {
			return 'announced'
		},
		/**
		 * Sign In
		 * @return {Promise} Promise, with result Firebase.User
		 */
		'signIn': function () {
			return self.auth.signInWithEmailAndPassword('display@qms.app', 'passwd')
		},
		/**
		 * Output text to device
		 * @param  {string} text [description]
		 * @return {[type]}      [description]
		 */
		'setDeviceText': function (deviceId, text) {
			return self.groupDataRef.child(deviceId).child('buffer').child('content').set( text || '')
		},

		'getDeviceText': function (deviceId, onReturn) {
			return self.groupDataRef.child(deviceId).child('buffer').child('content')
				.once('value', function (snapshot) {
					onReturn(snapshot.val())
				})
		},

		/**
		 * Apply state to selected device
		 * @param {string} stateCode
		 */
		'setDeviceState': function (deviceId, stateCode) {
			if (DisplayManager.POSSIBLE_STATES.indexOf(stateCode) < 0)
				throw new Error('Invalid state!');
			return self.groupDataRef.child(deviceId).child('buffer').child('state').set(stateCode)
		},

		'getDeviceState': function (deviceId, onReturn) {
			return self.groupDataRef.child(deviceId).child('buffer').child('state')
				.once('value', function (snapshot) {
					onReturn(snapshot.val())
				})
		},

		'registerDevice': function (deviceId) {
			return self.groupDataRef.child(deviceId).set({
				'buffer': {
					'content': 'TEST',
					'state': 'state-ready'
				},
				'control': {

				}
			})
		},

		'cancelDeviceRegistration': function (deviceId) {
			return self.groupDataRef.child(deviceId).set(null)
		},

		'trace': function () {
			return new Promise(function (resolve, reject) {
				// body... self.groupDataRef.
				self.groupDataRef.once('value', function (snapshot) {
					var collection = snapshot.val();
					if (collection) {
						var queue = []
						for (id in collection) {
							if (collection.hasOwnProperty(id)) {
								queue.push(self.groupDataRef.child(id).child('control/request/command').set('TRACE'))
							}
						}
						Promise.all(queue).then(resolve).catch(reject)
					} else resolve('Nothing to trace')
				})
			})
		},

		'pingDevice': function (deviceId) {
			var timeout = 5000; //
			var targerRef = self.groupDataRef.child(deviceId).child('echo');

			return new Promise(function (resolve, reject) {
				// Set watchdog timer to cancel ping on timeout:
				var doReject = reject;
				setTimeout(function () {
					// Cancel subscribtion:
					targerRef.child('pong').off('value')
					doReject();
				}, timeout);

				var requestTimestamp = +(new Date());
				targerRef.child('ping').set(requestTimestamp);
				targerRef.child('pong').on('value', function (snapshot) {
					var responseTimestamp = snapshot.val();
					console.warn('pong is:', responseTimestamp)
					if (responseTimestamp !== null) {
						var roundTripDealy = +(new Date()) - responseTimestamp;
						resolve(roundTripDealy)
						// Cancel subscribtion:
						targerRef.child('pong').off('value')
						targerRef.child('pong').set(null) // <-- clear response
					}
				})
			})
		},

		'connectRemoteData': function () {

			self.groupDataRef = self.database.ref(self.devicesDataPath());
			// Setup listener:
			self.groupDataRef.off()
			self.groupDataRef.on('value', self.handleGroupChanges)

			// ---
			var announceRef = self.database.ref(self.announcePath());
			announceRef.off()
			announceRef.on('value', self.handleAnnounceChanges)
			// announceRef.on('child_removed', self.handleAnnounceChanges)
			if (self.onConnected) self.onConnected()
		},

		'disconnectRemoteData': function () {
			self.groupDataRef = null;
		},

		'onAuthStateChanged': function (user) {
			if (user) {
				self.connectRemoteData()
				setAppState('state-signed-on')
			} else {
				self.disconnectRemoteData()
				setAppState('state-signed-off')
			}
		},
		/**
		 * To be called on event 'data changed in DB'
		 * @param  {object} data Firebase snapshot - object with .val() method
		 * @return {undefined}      None
		 */
		'handleGroupChanges': function (snapshot) {
			self.groupCollection = snapshot.val();
			if (self.groupCollection === null) {
				// Device list not found!
				console.error('Cannot find devices data')
			} else {
				var count = 0;
				// count length of displays collection ...
				for (var deviceId in self.groupCollection) {
					if (self.groupCollection.hasOwnProperty(deviceId)) count++
				}
				// notify about changes only if count of displays changed...
				if (self.deviceCount !== count) {
					self.deviceCount = count
					if (self.onListChanged) self.onListChanged()
				}
			}
		},

		'handleAnnounceChanges': function (snapshot) {
			self.announceCollection = snapshot.val()
			console.log('*handleAnnounceChanges', self.announceCollection)
			if (self.onListChanged) self.onListChanged()
		},

		'announceListToOptions': function () {
			var html = ['<option>Select...</option>'];
			if (self.announceCollection) {
				for (var deviceId in self.announceCollection) {
					if (self.announceCollection.hasOwnProperty(deviceId)) {
						var isSelected = (deviceId === selectedDeviceId);
						html.push('<option value="'+deviceId+'" '+((isSelected) ? ' selected':'')+'>'+deviceId+'</option>')
					}
				}
			}
			return html.join('')
		},

		'deviceListToOptions': function (selectedDeviceId) {
			var html = ['<option>Select...</option>'];
			if (self.groupCollection) {
				for (var deviceId in self.groupCollection) {
					if (self.groupCollection.hasOwnProperty(deviceId)) {
						var isSelected = (deviceId === selectedDeviceId);
						html.push('<option value="'+deviceId+'" '+((isSelected) ? ' selected':'')+'>'+self.groupCollection[deviceId].ADDR+'</option>')
					}
				}
			}
			return html.join('')
		},

		'statesToOptions': function () {
			// body...
			var html = ['<option>Select...</option>'];
			DisplayManager.POSSIBLE_STATES.forEach(function (state) {
				html.push('<option value="'+state+'">'+state+'</option>')
			})
			return html.join('')
		}

	})

	self.auth.onAuthStateChanged(self.onAuthStateChanged)

	self.signIn().catch(function (reason) {
			console.error('Auth error: ', reason)
			setAppState('state-signed-off')
		});

	return self;
}


module.exports = DisplayManager;


