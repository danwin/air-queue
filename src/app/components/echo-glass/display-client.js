// persistent.js
/*
Interacts with its reflection in DB
 */

var Magneto = require('./../interfaces/magneto');

var IPersistent = require('./../interfaces/persistent');

var DisplayNode = require('./display-node');
// var IQueueClient = require('./../interfaces/queue-client');

// var IHourglass = require('./../interfaces/hourglass');
// var DisplayClient = require('./../echo-glass/display-client');
// var PnPManager = require('./pnp-manager');

// var dialogWindow = require('./../utils/dialog.window');

var _ = require('underscore');

var thisModName = 'DisplayClient';

// Import:
// Export:
Magneto[thisModName] = DisplayClient;

var _extend = Magneto.extend;

DisplayClient.POSSIBLE_STATES = DisplayNode.POSSIBLE_STATES;

function DisplayClient(data) {
	data = data || {};
	var self = {}

	// Do not apply interface twice!:
	if (self[thisModName]) return;
	self[thisModName] = true;

	// Implements interface "Authentic":
	IPersistent(self, data);

	_extend(self, {
		'deviceType': data.deviceType || 'device-display',
		'busDirectory': data.busDirectory || 'PnPBus',

		'dataDirectory': 	data.dataDirectory || 'displays',
		'displayId': data.displayId || null
	})

	// if (data.displayId === void 0) Magneto.raise ('required attribute "displayId" missed!');

	var waitForResult = true;
	var timeoutMs = 15000;

	/**
	 * Allows to pass deviceId to subsequent .then() in the promise chain
	 * @param  {[type]} deviceId [description]
	 * @return {[type]}          [description]
	 */
	var selectChannel = function (deviceId) {
		var id = deviceId
		return function () {
			return id
		}
	}

	var instanceRef = function (deviceId) {
		console.log('<===> working with Display channel:',self.database
			.ref(self.dataDirectory)
			.child(deviceId).toString())
		return self.database
			.ref(self.dataDirectory)
			.child(deviceId)
	}

	var checkResponse = function (deviceId) {
		// Listen to response:
		if (!waitForResult) {
			return Promise.resolve(true);
		}

		var targetRef = instanceRef(deviceId).child('control/response');

		console.log('*** waiting for response***', deviceId, targetRef.toString())


		return new Promise(function(resolve, reject){

			var cancelOnTimeout = function () {
				reject(Magneto.constants.ERR_AUTH_TIMEOUT)
			}

			setTimeout(cancelOnTimeout, timeoutMs)

			targetRef.once('value', function (snapshot) {
				var response = snapshot.val();
				if (!response) {
					reject('no response!')
				} else if (response.ACK) {
					targetRef.remove(); // clear response
					resolve(response.ACK)
				} else if (response.NACK) {
					targetRef.remove(); // clear response
					reject(response.NACK)
				} else console.warn('unknown response from Display:', response)
			})
		})

	}

	_extend(self, {
		'getKey': function () {return self.displayId},

		'send': function (deviceId, text, state, waitForResult) {
			// To-do: check "sender" mark in ACK-NACK to avoid handling "alien" response
			var targetRef = instanceRef(deviceId).child('buffer')
			return targetRef.set({
					'content': text,
					'state': state
				})
				.then(selectChannel(deviceId))
				.then(checkResponse)
		},

		'ping': function (deviceId) {
			var timeout = 5000; //
			var targetRef = instanceRef(deviceId);

			return new Promise(function (resolve, reject) {
				// Set watchdog timer to cancel ping on timeout:
				var doReject = reject;
				setTimeout(function () {
					// Cancel subscribtion:
					targetRef.child('echo/pong').off('value')
					doReject();
				}, timeout);

				var listenEcho = function () {
					targetRef.child('echo/pong').on('value', function (snapshot) {
						var responseTimestamp = snapshot.val();
						console.warn('pong is:', responseTimestamp)
						if (responseTimestamp !== null) {
							var roundTripDealy = +(new Date()) - responseTimestamp;
							resolve(roundTripDealy)
							// Cancel subscribtion:
							targetRef.child('echo/pong').off('value')
							targetRef.child('echo/pong').set(null) // <-- clear response
						}
					})
				}

				var requestTimestamp = +(new Date());
				console.warn('PING PATH:', targetRef.child('echo/ping').toString());
				targetRef.child('echo/ping')
					.set(requestTimestamp)
					.then(listenEcho)
					.catch(Magneto.handleException);
			})
		},

		'trace': function (deviceId) {
			var targetRef = instanceRef(deviceId).child('control/request/command')
			console.log('command client -> ', targetRef.toString())
			return targetRef
				.set('TRACE')
				.then(selectChannel(deviceId))
				.then(checkResponse)
				.catch(Magneto.handleException)
		}

		// 'startObserveAddrList': function (callback) {
		// 	self.groupDataRef = self.database.ref(self.busDirectory + '/' + self.deviceType);
		// 	self.groupDataRef.off()
		// 	// Setup listener:
		// 	self.groupDataRef.on('value', function(snapshot){
		// 		var value = snapshot.val() || {};
		// 		callback(value)
		// 	})
		// },
		// 'stopObserveAddrList': function(callback) {
		// 	self.groupDataRef = self.database.ref(self.busDirectory + '/' + self.deviceType);
		// 	self.groupDataRef.off()
		// },
		// 'startObserveUUIDList': function(callback) {
		// 	self.groupDataRef = self.database.ref(self.dataDirectory);
		// 	self.groupDataRef.off()
		// 	// Setup listener:
		// 	self.groupDataRef.on('value', function(snapshot){
		// 		var value = snapshot.val() || {};
		// 		callback(value)
		// 	})
		// },
		// 'stopObserveUUIDList': function() {
		// 	self.groupDataRef = self.database.ref(self.dataDirectory);
		// 	self.groupDataRef.off('value')
		// }



	})

	return self;

}

module.exports = DisplayClient;