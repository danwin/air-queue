
//display.js
/*
To-do: implement HardwareId for asset management and connection protection.

To-do: Client device protection - via Auth for devices. Create tokens via JS RSA lib. (see: http://kjur.github.io/jsrsasign/; https://github.com/firebase/quickstart-js/blob/master/auth/exampletokengenerator/auth.html)

To-do: own namespaces for each interface? e.g.: self.ifControllable.request()
To-do: internal "on()" - for
To-do: move ping from "control" to "echo", change ping/pong also to request/response

 */

var Magneto = require('./../interfaces/magneto');

var IControllable = require('./../interfaces/controllable');

var IStateful = require('./../interfaces/stateful');
var IPingable = require('./../interfaces/pingable');
var IOnline = require('./../interfaces/online');

var IPnP = require('./../interfaces/pnp');
var IMenu = require('./../interfaces/menu');
// var IQueueClient = require('./../interfaces/queue-client');

// var IHourglass = require('./../interfaces/hourglass');
// var DisplayClient = require('./../echo-glass/display-client');
// var PnPManager = require('./pnp-manager');

// var dialogWindow = require('./../utils/dialog.window.js');

var _ = require('underscore');


var thisModName = 'DisplayNode';

var _extend = Magneto.extend;
var wait = Magneto.wait;

// Export:
Magneto[thisModName] = DisplayNode;


// Перечисляем "допустимые" состояния для табло. Обрати внимание - значения "совместимы" с именами классов CSS - начинаются не с цифры и т.п. Этот массив просто для проверки что мы присваиваем "законное" состояние, предусмотренной в системе (используя [...].indexOf(state) >-1 )
var POSSIBLE_STATES = {
	'state-trace': true, /* New device - no registration record in database*/
	'state-service-menu': true, /* Show on-screen menu*/
	'state-idle': true, /* */
	'state-ready': true,
	'state-calling': true, /*  */
	'state-customer-session': true, /*  */
	'state-session-done': true,
	'state-maintenance': true,
	'state-error': true,
	'state-signed-off': true, /* Authentication failure */
	'state-offline': true /* Network problem - no connection */
	}

// As a class-wide constant:
DisplayNode.POSSIBLE_STATES = POSSIBLE_STATES;

function DisplayNode(data) {
	data = data || {}
	var self = {}

	// Properties

	// Setting for Properties:
	data.deviceType = data.deviceType || 'device-display';
	data.busDirectory = data.busDirectory || 'PnPBus';
	data.regDirectory = data.regDirectory || 'registry';
	data.dataDirectory = data.dataDirectory || 'displays';
	data.storageKeyForDeviceId = data.storageKeyForDeviceId || 'mg-device-id';
	data.POSSIBLE_STATES = POSSIBLE_STATES,

	IControllable(self, data)
	IStateful(self, data)
	IPnP(self, data)
	IMenu(self, data)
	IOnline(self, data)
	IPingable(self, data)

	// New properties:
	_extend(self, {
		'credentials': {
			'email': 'display@qms.app',
			'password': 'passwd'
		}
	})

	// New Methods:
	_extend(self, {
		'setText': function (text) {
			var targets = document.querySelectorAll('.display-text');
			[].forEach.call(targets, function (el) {
				el.innerHTML = text || ''
			})
		}
	});


	/**
	 * To be called on event 'data changed in DB'
	 * @param  {object} data Firebase snapshot - object with .val() method
	 * @return {undefined}      None
	 */
	var handleBufferChanges = function (snapshot) {
		var timestamp = (new Date()).getTime();
		try {
			var buffer = snapshot.val();
			if (buffer === null) {
				// Device is not registered or registration removed
				// console.log('CHECK DEVICE REGISTRATION')
				// var key = self.getKey();
				// var maskedId = key.substring(key.length-6)
				// self.setText(maskedId);
				// self.setState('state-trace');
			} else {
				console.log('data received', buffer)
				self.setText(buffer.content)
				self.setState(buffer.state);
				self.ACK(timestamp)
				// lastChangesTimestamp = timestamp;
			}
		} catch (e) {
			self.NACK(timestamp, e.message)
			console.error(e);
			self.setState('state-error')
		}
	}

	var commandDispatcher = function (command, args) {
		console.log('DisplayNode->commandDispatcher called', command, args)
		switch (command) {
			case 'TRACE':
				console.warn('commandDispatcher: command', command);
				// Use pushText, pushState, popState, popText; run TRACE only XXX seconds
				self.setText(self.getAddress());
				self.setState('state-trace');
				return true; // command recognized and performed
			}
	}


	// // Initialize object:
	// console.log('--- init ---')
	// self.listenToChildNode ('buffer', handleBufferChanges, 'value')

	// self.installCommandDispatcher(commandDispatcher);

	// self.on('state-pnp-done', init)
	// self.on('state-pnp-new-address', init)

	self.on('state-pnp-done', function () {
		var setNewState = function () {
			self.setState('state-ready');
			document.title = document.title.replace(/\[.*\]/g, '['+(self.deviceAddr || '-') +']')

		// Initialize object:
		console.log('--- init ---')
		self.listenToChildNode ('buffer', handleBufferChanges, 'value')

		self.installCommandDispatcher(commandDispatcher);

		}
		wait(5000).then(setNewState).catch(Magneto.handleException)
	})

	var autoSignIn = function () {
		return self.signIn(self.credentials)
	}

	// retrieveDeviceId();
		self.retrieveSession()
			.catch(autoSignIn)
			.then(function () {
				console.log('Magneto Display:  Signed!')
			})
			.catch(function (reason) {
				console.error('Auth error: ', reason)
				self.setState('state-signed-off')
			});

	return self;
}


module.exports = DisplayNode;