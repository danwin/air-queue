// steward.js

var Magneto = require('./../interfaces/magneto');

var IPersistent = require('./../interfaces/persistent');

var IStateful = require('./../interfaces/stateful');
var IPingable = require('./../interfaces/pingable');
var IOnline = require('./../interfaces/online');

var IPnP = require('./../interfaces/pnp');
var IMenu = require('./../interfaces/menu');

var IQueueClient = require('./../interfaces/queue-client');

var IHourglass = require('./../interfaces/hourglass');
// var DisplayClient = require('./../echo-glass/display-client');
// var PnPManager = require('./pnp-manager');

// var dialogWindow = require('./../utils/dialog.window');

var _ = require('underscore');

var thisModName = 'Steward';

var _extend = Magneto.extend;

// Export:
Magneto[thisModName] = Steward;


// Перечисляем "допустимые" состояния для терминала-регистратора.
var POSSIBLE_STATES = {
	'state-trace': true, /* New device - no registration record in database*/
	'state-service-menu': true, /* Show on-screen menu*/
	'state-idle': true, /* */
	'state-ready': true,

	'state-selection': true,
	'state-enter-person-id': true,
	'state-display-ticket-info': true,

	'state-maintenance': true,
	'state-error': true,
	'state-signed-off': true, /* Authentication failure */
	'state-offline': true /* Network problem - no connection */
	}


function Steward(data) {
	data = data || {}
	// Properties
	var self = {}

	// Setting for Properties:
	data.deviceType = 'device-ticket-station';
	data.busDirectory = 'PnPBus';
	data.dataDirectory = data.dataDirectory || 'ticket-devices';
	data.storageKeyForDeviceId = data.storageKeyForDeviceId || 'device-id';
	data.POSSIBLE_STATES = POSSIBLE_STATES;

	self.credentials = {
		'email': 'reception@qms.app',
		'password': 'passwd'
	}

	IPersistent(self, data)
	IPingable(self, data)
	IStateful(self, data)
	IPnP(self, data)
	IMenu(self, data)
	IOnline(self, data)
	IHourglass(self, {'houglassSelector': '.ajax-wait'})
	IQueueClient(self, data)

	// New properties:
	// New properties:
	_extend(self, {
		'ticketDisplayText': {},
		'basket': {},
		'metadata': {}
	});



	// Create listeners
	// Select all elements with attribute (more: https://developer.mozilla.org/en/docs/Web/CSS/Attribute_selectors)

	var selectors = document.querySelectorAll('*[data-action-select-service]');

	[].forEach.call(selectors, function (ctrlEl) {
		var serviceId = ctrlEl.getAttribute('data-action-select-service');
		ctrlEl.addEventListener('click', function () {
			self.basket[serviceId] = true
		})
	})

	// Select all elements with attribute (more: https://developer.mozilla.org/en/docs/Web/CSS/Attribute_selectors)

	// !!! This action will be done by IStateful!:
	// var jumpers = document.querySelectorAll('*[data-action-set-state]');

	// [].forEach.call(jumpers, function (ctrlEl) {
	// 	var stateId = ctrlEl.getAttribute('data-action-set-state');
	// 	ctrlEl.addEventListener('click', function () {
	// 		self.setState(stateId)
	// 	})
	// })



	var submitters = document.querySelectorAll('*[data-action-code="register-ticket"]');
	[].forEach.call(submitters, function (ctrlEl) {
		var ticketDisplay = document.querySelector('*[data-display-value="ticket-no"]')

		var displayTicketInfo = function () {
			ticketDisplay.innerHTML = self.ticketDisplayText
		}

		var nextState = function () {
			self.setState('state-display-ticket-info')
		}

		var input = document.querySelector('input[data-set-value="person-id"]');
		if (input) {
			ctrlEl.addEventListener('click', function () {
				self.ticketDisplayText = input.value;
				input.value = ''; // Clear value

				ticketDisplay.innerHTML = 'Wait, please...'

				self.beginWait(); // <-- "Hourglass"
				self.createTicket({
					'ticketDisplayText' : self.ticketDisplayText,
					'basket' : self.basket || {},
					'metadata' : self.metadata
				})
				.then(displayTicketInfo)
				.then(nextState)

			})
		} else console.error('Cannot find element "#'+id+'" from "data-action-register-ticket" attribute!');
	})

	// Translate PnP states:
	self.on('state-pnp-done', function () {self.setState('state-selection')})
	self.on('state-pnp-new-address', function () {self.setState('state-selection')})
	self.on('state-online', function () {self.setState('state-selection')})

	// Handle entrance in state "state-display-ticket-info":
	self.on('state-enter-person-id', function () {
		var input = document.querySelector('input[data-set-value="person-id"]');
		if (input) {
			input.value = '';
			input.focus()
		}
	})

	// Handle entrance in state "state-display-ticket-info":
	self.on('state-display-ticket-info', function () {
		function setWatchdog() {
			var remains = 5;
			var interval = setInterval(function(){
				remains--;
				var info_node = document.querySelector('span.info.time-tick-remains')
				if (info_node) info_node.innerHTML = remains+' sec'
			}, 1000); // 1 second
			setTimeout(function(){
				clearInterval(interval)
				// Clear user data from previous session:
				self.ticketDisplayText = '';
				self.basket = {}
				// Jump to initial state:
				self.setState('state-selection')
			}, 5000); // Timeout 5 seconds
		}
		self.endWait(); // <-- hide "Hourglass"
		setWatchdog()
	})

	var autoSignIn = function () {
		return self.signIn(self.credentials)
	}

	self.initUI = function () {
		// body...
	}

	// retrieveDeviceId();
	self.retrieveSession()
		.catch(autoSignIn)
		.then(function () {
			console.log('Magneto Ticket device:  Signed!');
			// Enter to the start state:
			self.setState('state-selection')
		})
		.catch(function (reason) {
			console.error('Auth error: ', reason)
			self.setState('state-signed-off')
		});

	console.log('steward: ', self);

	// install input handlers and virtual keyboard:
	(function () {
		/* Export handlers for input*/

		var _stopEvent = function (e) { //Stop the event
			//e.cancelBubble is supported by IE - this will kill the bubbling process.
			e.cancelBubble = true;
			e.returnValue = false;

			//e.stopPropagation works in Firefox.
			if (e.stopPropagation) {
				e.stopPropagation();
				e.preventDefault();
			}
			return false
		}

		var validateInput = function (evt) {
			var e = evt ? evt : window.event;
			var key = e.charCode || e.keyCode || e.which;
			var keyChar = String.fromCharCode(key);
			var text = this.value;
			console.log(text, key, keyChar);
			// Digits only: key = [48, ... 57]
			if (key == 8) {
				return true
			} else if (key>46 && key<58 && text.length < 4) {
				return true
			} else {
				_stopEvent(e)
				return false;
			}
		}

		function updateControls() {
			try {
				var targetInput = document.querySelector('input[data-set-value="person-id"]');
				var data = targetInput ? targetInput.value : '';
				var nextBtn = document.getElementById('create-ticket');
				console.log('inputCalled', data);
				if (data.length === 4) {
					nextBtn.removeAttribute('disabled');
				} else {
					nextBtn.setAttribute('disabled', true);
				}
			} catch (e) {console.error(e)}
		}

		var inputs = document.querySelectorAll('input[data-set-value="person-id"]');
		[].forEach.call(inputs, function (element) {
			element.addEventListener('keypress', validateInput)
			element.addEventListener('change', updateControls)
		})

		/*
		Virtual keyboard
			*/
		var inputChar = function () {
			var element = this;
			var char = element.getAttribute('data-keyboard-button')
			var targetInput = document.querySelector('input[data-set-value="person-id"]');
			if (targetInput && targetInput.value.length < 4) targetInput.value = targetInput.value + char
			updateControls()
		}
		var deleteLastChar  = function () {
			var element = this;
			var char = element.getAttribute('data-keyboard-button')
			var targetInput = document.querySelector('input[data-set-value="person-id"]');
			if (targetInput) {
				var value = targetInput.value
				if (value.length>0) targetInput.value = value.substring(0, value.length-1)
				updateControls()
			}
		}
		var inputs = document.querySelectorAll('*[data-keyboard-button]');
		[].forEach.call(inputs, function (element) {
			element.addEventListener('click', inputChar)
		})
		var inputs = document.querySelectorAll('*[data-keyboard-backspace]');
		[].forEach.call(inputs, function (element) {
			element.addEventListener('click', deleteLastChar)
		})

	})()

	return self;


}

module.exports = Steward;