var Magneto = require('./../interfaces/magneto');

var IPersistent = require('./../interfaces/persistent');
var IStateful = require('./../interfaces/stateful');
var IOnline = require('./../interfaces/online');

var IPnP = require('./../interfaces/pnp');
var IMenu = require('./../interfaces/menu');
var IQueueClient = require('./../interfaces/queue-client');

var IHourglass = require('./../interfaces/hourglass');
var DisplayClient = require('./../echo-glass/display-client');
var PnPManager = require('./pnp-manager');

var dialogWindow = require('./../utils/dialog.window');

var _ = require('underscore');


var thisModName = 'AdminNode';

var _extend = Magneto.extend;

// Export:
Magneto[thisModName] = AdminNode;

var labelForState = {
	'state-trace': 'Trace', /* New device - no registration record in database*/
	'state-service-menu': 'Menu On', /* Show on-screen menu*/
	'state-idle': 'Idle', /* */
	'state-ready': 'Ready',
	'state-calling': 'Calling', /*  */
	'state-customer-session': 'Customer', /*  */
	'state-maintenance': 'Maintenance',
	'state-error': 'Error',
	'state-signed-off': 'Signed out', /* Authentication failure */
	'state-offline': 'Offline', /* Network problem - no connection */

	'state-pnp-connecting': 'PnP connect...',
	'state-pnp-done': 'PnP ready',
	'state-pnp-new-address': 'PnP (got new address)',
	'state-pnp-address-conflict': 'Error (PnP address conflict)',
	'state-pnp-bus-overflow': 'Error (PnP bus overflow)',
	'state-pnp-unknown-error': 'Error (PnP)'
}

// Перечисляем "допустимые" состояния
var POSSIBLE_STATES = {

	'state-edit-workplaces': true,
	'state-edit-displays': true,
	'state-edit-ticket-devices': true,
	'state-edit-users': true,


	'state-trace': true, /* New device - no registration record in database*/
	'state-idle': true, /* */

	'state-ready': true,

	'state-maintenance': true,
	'state-error': true,
	'state-signed-off': true, /* Authentication failure */
	'state-offline': true /* Network problem - no connection */
	}

var SERVICE_LABELS = {
	'deposit': 'Deposit operations',
	'loan-application': 'Loan application',
	'cash-out': 'Cash out',
	'consult': 'Consult'
}


function AdminNode(data) {
	data = data || {}
	// Properties
	var self = {}

	// Setting for Properties:
	data.deviceType = 'device-admin-console';
	data.busDirectory = 'PnPBus';
	data.dataDirectory = data.dataDirectory || 'admin-nodes';
	data.storageKeyForDeviceId = data.storageKeyForDeviceId || 'device-id';
	data.POSSIBLE_STATES = POSSIBLE_STATES;

	self.credentials = {
		'email': 'manager@qms.app',
		'password': 'passwd'
	}

	IPersistent(self, data)
	IStateful(self, data)
	IPnP(self, data)
	IOnline(self, data)
	IHourglass(self, {'houglassSelector': '.ajax-wait'})

	// Note IQueueClient has another dataDirectory so make a separate object for it (do not apply this interface to "self" directly)!
	self.qClient = {};
	var qcData = _extend({}, data);
	qcData.dataDirectory = 'ticket-devices';
	IQueueClient(self.qClient, qcData)

	self.qClient.getKey = self.getKey; // use the same key function for queue client

	self.dClient = DisplayClient({
		'dataDirectory': 'displays'
	});

	self.displaysManager = PnPManager({
		'deviceType': 'device-display',
		'dataDirectory': 'displays',
		'busDirectory': 'bus',
		'regDirectory': 'registry'
	})

	self.ticketsManager = PnPManager({
		'deviceType': 'device-ticket-station',
		'dataDirectory': 'ticket-devices',
		'busDirectory': 'bus',
		'regDirectory': 'registry'
	})

	self.workplacesManager = PnPManager({
		'deviceType': 'device-operator-console',
		'dataDirectory': 'operator-nodes',
		'busDirectory': 'bus',
		'regDirectory': 'registry'
	})

	self.enumDevicesByUUID = function (managerInstance) {
		return managerInstance
			.getRegistryRef()
			.once('value')
			.then(function (snapshot) {
				return snapshot.val()
			})
	};

	// Query which shows unused displays:

	// Query which shows attached displays:


	// New properties:
	_extend(self, {
		'queueBuffer': [], /* Current state of queue */
		'selectedTicket': null, /* Selected ticket */
		'handleException': function (error) {
			console.error('handleException: ', error)
		}
	});

	var displayQueueLength = function () {
		var len = self.queueBuffer.length;
		var placeholders = document.querySelectorAll('*[data-display-value="queue-length"]');
		[].forEach.call(placeholders, function (el) {
			if (len > 0) {
				// show badge if queue is non-empty and display length:
				el.innerHTML = len
				el.style.display = ''
			} else {
				// hide badge if queue is empty:
				el.style.display = 'none'
			}
		})
	}

	/*
	Status messages
		*/
	var displayStatusMessage = function (message) {
		var placeholders = document.querySelectorAll('*[data-display-value="status-message"]');
		[].forEach.call(placeholders, function (el) {
			el.innerHTML = message
				+ '&nbsp;<button class="btn btn-xs btn-primary" data-action-code="hideStatusMessage">Ok</button>'
		})
	}
	document.getElementsByTagName('body')[0].addEventListener('click', function (event) {
		var e = event || window.event;
		if (e.target.getAttribute('data-action-code') === 'hideStatusMessage') {
			var elements = document.querySelectorAll('*[data-display-value="status-message"]');
			[].forEach.call(elements, function (el) {
				el.innerHTML = ''
			})
		}
	})

	/**
	 * [runTimer description]
	 * @param  {[type]} targetSelector [description]
	 * @param  {[type]} stopAtSecond   [description]
	 * @return {[type]}                [description]
	 */
	var runTimer = function (targetSelector, stopAtSecond) {
		return new Promise(function (resolve, reject) {
			var elapsed = 0; // How many seconds elapsed
			var intervalMs = 1000; // <-- 1 sec
			var lastSecond = stopAtSecond || 60; // <-- kill timer after reaching this value;
			var _2digits = function (value) {
				return (value < 10) ? '0'+value : value;
			}
			var selector = targetSelector;

			function iterateSecond () {
				var now = new Date();
				var time = new Date(0,0,0,0,0, elapsed); // <-- use only "seconds" argument
				var hours = time.getHours(),
					minutes = time.getMinutes(),
					seconds = time.getSeconds(),
					timeStr = [
							_2digits(hours),
							_2digits(minutes),
							_2digits(seconds)
						].join(':');

				[].forEach.call(document.querySelectorAll(selector), function (el) {
					el.innerHTML = timeStr
				});
				elapsed++; // <-- next value for seconds counter;
				if (elapsed < lastSecond) {
					// Call "iterateSecond" recursively, with time compensation:
					setTimeout(iterateSecond, intervalMs - (now % intervalMs))
				} else {
					// Resolve promise after timeout!:
					resolve(elapsed)
				}
			}

			// Start chain:
			iterateSecond()
		})
	}


	// Highight active menu item:
	var setActiveMenuItem = function (state) {
		var items = document.querySelectorAll('ul#pages-nav li');
		var currentItem = document.querySelector('ul#pages-nav a[data-action-set-state="'+state+'"]');
		[].forEach.call(items, function (item) {item.className = ''});
		if (currentItem && currentItem.parentNode) currentItem.parentNode.className = 'active';
	}

	// Translate PnP states:
	self.on('state-pnp-done', function () {self.setState('state-ready')})
	self.on('state-pnp-new-address', function () {self.setState('state-ready')})
	self.on('state-online', function () {self.setState('state-ready')})


	// self.on('state-ready', function () {
	// 	self.dClient.send(
	// 		'--:--',
	// 		'state-ready',
	// 		false);
	// 	displayStatusMessage('Ready');
	// })

	self.on('state-edit-users', function () {
		document.title = 'Users | QMS Dashboard'
		setActiveMenuItem('state-edit-users')
		// enumerate users
	})

	self.on('state-edit-displays', function () {
		document.title = 'Displays | QMS Dashboard'
		setActiveMenuItem('state-edit-displays')
		// enumerate displays
		self.displaysManager.startObserveData(function(registry){
			console.log('startObserveData called', registry)
			var list = [], rows = [];
			registry = registry || {}; // argument can be null
			for (var uuid in registry) {
				var record = registry[uuid];
				if (registry.hasOwnProperty(uuid) && record.ADDR) {
					// extend with uuid property:
					record.UUID = uuid;
					var index = parseInt(record.ADDR);
					list[index] = record
					console.log('[]', index, record.ADDR)
				}
			}

			var template = document.getElementById('table-displays-row').innerHTML
			// Parse list (address always strats from 1!):
			for (var i=1, len=list.length; i<len; i++ ) {
				var item = list[i];
				if (!item) continue; // for unused Address
				rows.push(
					'<div class="row">\
						<div class="col-xs-1">\
							Display #{ADDR}\
						</div>\
						<div class="col-xs-3">\
							{UUID}\
						</div>\
						<div class="col-xs-2">\
							{online}\
						</div>\
						<div class="col-xs-2">\
							{state}\
						</div>\
						<div class="col-xs-4 row-buttons">\
							<button class="btn btn-info btn-xs" data-action-code="pingDisplay" data-action-args="{UUID}">Ping</button>\
							<button class="btn btn-primary btn-xs" data-action-code="traceDisplay" data-action-args="{UUID}">Trace</button>\
							<button class="btn btn-primary btn-xs" data-action-code="testDisplay" data-action-args="{UUID}">Test output</button>\
						</div>\
					</div>'
					.replace(/{ADDR}/g, i)
					.replace(/{UUID}/g, item.UUID)
					.replace(/{state}/g, (item.buffer) ? labelForState[item.buffer.state] : '-')
					.replace(/{online}/g, item.online ? '<span class="badge bg-info">online</span>' : '<span class="badge">offline</span>')

					// .replace('{timestamp}', (new Date(ticket.createdAt)).toLocaleTimeString())
					// .replace('{info}', fmtBasket(ticket.basket))
				)
			}
			document.getElementById('displays').innerHTML = rows.join('')
		})
	})

	self.on('state-edit-ticket-devices', function () {
		document.title = 'Tickets | QMS Dashboard'
		setActiveMenuItem('state-edit-ticket-devices')
		// enumerate ticket devices
		self.ticketsManager.startObserveData(function(registry){
			console.log('startObserveData called', registry)
			var list = [], rows = [];
			registry = registry || {}; // argument can be null
			for (var uuid in registry) {
				var record = registry[uuid];
				if (registry.hasOwnProperty(uuid) && record.ADDR) {
					// extend with uuid property:
					record.UUID = uuid;
					var index = parseInt(record.ADDR);
					list[index] = record
					console.log('[]', index, record.ADDR)
				}
			}
			var template = document.getElementById('table-ticket-devices-row').innerHTML
			// Parse list (address always strats from 1!):
			for (var i=1, len=list.length; i<len; i++ ) {
				var item = list[i];
				if (!item) continue; // for unused Address
				rows.push(
					template
						.replace(/{ADDR}/g, i)
						.replace(/{UUID}/g, item.UUID)
						.replace(/{online}/g, item.online ? '<span class="badge bg-info">online</span>' : '<span class="badge">offline</span>')

						// .replace('{timestamp}', (new Date(ticket.createdAt)).toLocaleTimeString())
						// .replace('{info}', fmtBasket(ticket.basket))
				)
			}
			document.getElementById('ticket-devices').innerHTML = rows.join('')
		})
	})
	self.on('state-edit-workplaces', function () {
		document.title = 'Workplaces | QMS Dashboard'
		setActiveMenuItem('state-edit-workplaces')
		// enumerate operator consoles
		var enumWorkplaces = function (displays) {
			return new Promise(function (resolve, reject) {
				self.workplacesManager.startObserveData(function(registry){
					// Lookup diplay address by uuid:
					var displaysByUUID = displays;

					console.log('*displays llokup*', displays)
					console.log('startObserveData called', registry)

					var list = [], rows = [];
					registry = registry || {}; // argument can be null
					_.each(registry, function (record, uuid) {
						record.UUID = uuid
						record.settings = record.settings || {}
						if (record.settings.displayId) {
							record.assignedDisplay = 'Display #' + displaysByUUID[record.settings.displayId].ADDR || '-';
						}
						list[parseInt(record.ADDR)] = record
					})
					var template = document.getElementById('table-workplaces-row').innerHTML

					// Parse list (address always strats from 1!):
					for (var i=1, len=list.length; i<len; i++ ) {
						var item = list[i];
						if (!item) continue; // for unused Address
						rows.push(
							template
								.replace(/{ADDR}/g, i)
								.replace(/{UUID}/g, item.UUID)
								.replace(/{online}/g, item.online ? '<span class="badge bg-info">online</span>' : '<span class="badge">offline</span>')
								.replace(/{display}/g, item.assignedDisplay)
						)
					}
					document.getElementById('workplaces').innerHTML = rows.join('')
					resolve(true)
				})
			})
		}

		// enumerate displays, then workplaces
		self.enumDevicesByUUID(self.displaysManager)
			.then(enumWorkplaces)
			.catch(Magneto.handleException)
	})
// - - -
//

	self['pingDisplay'] = function (uuid) {
		console.log('pingDisplay')
		displayStatusMessage('Pinging device...')
		self.dClient.ping(uuid)
			.then(function (delayMs) {
				displayStatusMessage('Device responds, delay (ms) is:' + delayMs, true)
			})
			.catch(function () {
				displayStatusMessage('No response - cancelling by timeout...')
			})
	}

	self['traceDisplay'] = function (uuid) {
		console.log('traceDisplay')
		displayStatusMessage('Tracing device...')
		self.dClient.trace(uuid)
			.then(function () {
				displayStatusMessage('Device in a trace mode and displays ID')
			})
			.catch(function () {
				displayStatusMessage('No response - cancelling by timeout...')
			})
	}

	self['assignDisplay'] = function (uuid) {
		// render list of options for <select>:
		var workplaceUUID = uuid;
		var DialogAssignDisplay = window.dialogWindow.fromTemplate('dialog-assign-display')

		var formatDialogBuffer = function (registry) {
			// render list of options for <select>:
			var rows = [];
			console.log('formatDialogBuffer', registry)
			for (var uuid in registry) {
				if (registry.hasOwnProperty(uuid)) {
					var value = uuid;
					var label = registry[uuid].ADDR;
					console.log('rendering options:', registry[uuid])
					if (label) rows.push(
						'<option value="{value}">Display #{label} [{value}]</option>'
							.replace(/{value}/g, value)
							.replace(/{label}/g, label)
						)
				}
			}
			return {
				'title': 'Select display',
				'displays': rows.join('')
			}
		}

		// Prepare data and show dialog:
		self.enumDevicesByUUID(self.displaysManager)
			.then(formatDialogBuffer)
			.then(DialogAssignDisplay.show)
			.then(function (result) {
				if (result) {
					var selectedUUID = document.querySelector('select[data-dialog-value="displays"]').value;
					console.log('selection: ', workplaceUUID, selectedUUID)
					self.workplacesManager.getDataRef()
						.child(workplaceUUID)
						.child('settings/displayId')
						.set(selectedUUID)
						.catch(Magneto.handleException)
				}
			})
			.catch(Magneto.handleException)
	}

	self['testDisplay'] = function (uuid) {
		var selectedUUID = uuid;
		var DialogTestDisplay = window.dialogWindow.fromTemplate('dialog-test-display-output')
		function formatStatesOptions(object) {
			var buffer = [];
			for (var name in object) {
				if (object.hasOwnProperty(name)) {
					buffer.push(
						'<option value="{state}" {selected}>{state}</option>'
							.replace(/{state}/g, name)
							.replace(/{selected}/g, (name==='state-ready') ? 'selected="true"' : ''))
				}
			}
			return buffer.join('')
		}
		var dialogBuffer = {
				'title': 'Output text for test',
				'states': formatStatesOptions(DisplayClient.POSSIBLE_STATES)
			}
		DialogTestDisplay.show(dialogBuffer)
			.then(function (result) {
				if (result) {
					var selectedState = document.querySelector('select[data-dialog-value="states"]').value;
					var text = document.querySelector('input[data-dialog-value="textString"]').value;
					console.log('testing display...: ', selectedUUID)

					displayStatusMessage('Output to display...')
					self.dClient.send(selectedUUID, text, selectedState)
						.then(function () {
							displayStatusMessage('Device responds, look on its screen', true)
						})
						.catch(function () {
							displayStatusMessage('No response')
						})
				}
			})
			.catch(Magneto.handleException)
	}

	// Create listeners
	// Select all elements with attribute (more: https://developer.mozilla.org/en/docs/Web/CSS/Attribute_selectors)
	// common dispatcher with delegation
	document.getElementsByTagName('body')[0].addEventListener('click', function (event) {
		var e = event || window.event;
		if (e.target.hasAttribute('data-action-code')) {
			var targetAction = e.target.getAttribute('data-action-code');
			var targetArgs = e.target.getAttribute('data-action-args');
			if (typeof self[targetAction] === 'function') {
				self[targetAction](targetArgs)
			}
		}
	});


	/*
	Part BELOW: for re-factoring; use QuueClient ?
		*/



	var selectors = document.querySelectorAll('*[data-action-select-service]');

	[].forEach.call(selectors, function (ctrlEl) {
		var serviceId = ctrlEl.getAttribute('data-action-select-service');
		ctrlEl.addEventListener('click', function () {
			self.basket[serviceId] = true
		})
	})

	// Select all elements with attribute (more: https://developer.mozilla.org/en/docs/Web/CSS/Attribute_selectors)

	var ctrlCallFirst = document.querySelectorAll('*[data-action-code="call-first"]');
	[].forEach.call(ctrlCallFirst, function (ctrlEl) {
		ctrlEl.addEventListener('click', function () {
			var firstItem = self.queueBuffer[0];
			if (firstItem) {
				var ticketId = firstItem.ticketId;
				self.selectedTicket = firstItem;
				self.beginWait() // <-- IHourglass.beginWait()
				self.qClient.callTicket(ticketId)
					.then(function () {
						self.setState('state-calling')
					})
					.catch(self.handleException)
					.then(self.endWait) // <-- here ".then" acts as a "finally"
			}
		})
	})

	var jumpers = document.querySelectorAll('*[data-action-set-state]');

	[].forEach.call(jumpers, function (ctrlEl) {
		var stateId = ctrlEl.getAttribute('data-action-set-state');
		ctrlEl.addEventListener('click', function () {
			self.setState(stateId)
		})
	})

	var submitters = document.querySelectorAll('*[data-action-register-ticket]');
	[].forEach.call(submitters, function (ctrlEl) {
		var setting = ctrlEl.getAttribute('data-action-register-ticket');
		if (setting.match(/^\input#/)) {
			var id = setting.replace(/^input#/, '');
			console.log('submitters: ', setting, id);
			var input = document.getElementById(id);
			if (input) {
				ctrlEl.addEventListener('click', function () {
					self.ticketDisplayText = input.value;
				})
			} else console.error('Cannot find element "#'+id+'" from "data-action-register-ticket" attribute!')
		} else console.error('invalid value of attribute "data-action-register-ticket": ', setting)
	})


	var observeQueue = function (snapshot) {
		console.log(snapshot.val());
		var root = document.getElementById('queue');
		var queue = snapshot.val();
		var buffer = [];
		for (var id in queue) {
			if (queue.hasOwnProperty(id)) {
				var ticket = queue[id];
				// filter waiting tickets  with "state-new"
				if (ticket.state === 'state-new') {
					// Copy the ticket object and extend and add "ticketId" property:
					ticket.ticketId = id
					buffer.push(ticket)
				}
			}
		}
		// Sort array in descent order:
		buffer.sort(function (a, b) {
			return b.createdAt - a.createdAt
		})

		// Create DOM elements for array items
		var rows = []
		function fmtBasket(basket) {
			var buffer = [];
			for (var name in basket) {
				if (basket.hasOwnProperty(name)) buffer.push(SERVICE_LABELS[name])
			}
			return buffer.join(',')
		}

		// Update length indicator / badge:
		displayQueueLength()

		buffer.forEach(function (ticket) {
			rows.push(
				'<div class="row">\
					<div class="col-xs-3">\
						<span class="pic"></span>\
						<span class="ticket-display-text">{ticketDisplayText}</span>\
					</div>\
					<div class="col-xs-3">\
						{timestamp}\
					</div>\
					<div class="col-xs-6">\
						{info}\
					</div>\
				</div>'
				.replace(/{ticketDisplayText}/g, ticket.ticketDisplayText)
				.replace(/{timestamp}/g, (new Date(ticket.createdAt)).toLocaleTimeString())
				.replace(/{info}/g, fmtBasket(ticket.basket))
			)
		})
		// root.innerHTML = rows.join('');
		// Store a local copy:
		self.queueBuffer = buffer;
	}

	// Install queue observer:
	console.log('-1-')
	self.qClient.listenToQueue(observeQueue, 'value');
	console.log('-2-')

	var autoSignIn = function () {
		return	self.signIn(self.credentials)
			// self.qClient(self.credentials)
	}

	// retrieveDeviceId();

	self.beginWait()
	self.listenToReadyState(self.endWait)

	self.retrieveSession()
		.catch(autoSignIn)
		.then(function () {
			console.log('Magneto Operator node:  Signed!')
			// Enter to the start state:
			self.setState('state-ready')
		})
		.catch(function (reason) {
			console.error('Auth error: ', reason)
			self.setState('state-signed-off')
		})
		// .then(self.endWait);

	console.log('operator node: ', self)

	return self;


}

module.exports = AdminNode;