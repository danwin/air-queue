var Magneto = require('./../interfaces/magneto');

var IPersistent = require('./../interfaces/persistent');

var thisModName = 'PnPManager';

// Export:
Magneto[thisModName] = PnPManager;

var _extend = Magneto.extend;


/**
 * Implements PnP functions
 * @param {object} self Instance Magneto (defined)
 * @param {object} data Optional values for attributes
 */
function PnPManager(data) {
	var self = {}
	data = data || {};

	if (!data.deviceType) Magneto.raise('requires "deviceType" attribute!')

	IPersistent(self, data);

	// Do not apply interface twice!:
	if (self[thisModName]) return; //[] to-do: allow apply "data" multiple times!
	self[thisModName] = true;

	// New properties
	_extend(self, {
		'deviceType': data.deviceType || 'basic-device',
		'busDirectory': data.busDirectory || 'bus',
		'regDirectory': data.regDirectory || 'registry',
		'dataDirectory': data.dataDirectory || null,
		'maxAddrValue': data.maxAddrValue || Number.MAX_VALUE
		// 'deviceType': null
	})

	// To-do: use these 2 methods from IPnP!
	var getBusRef = function () {
		console.log('---> getBusRef', self.database.ref(self.busDirectory + '/' + self.deviceType).toString())
		return self.database.ref(self.busDirectory + '/' + self.deviceType)
	}
	var getRegistryRef = function () {
		console.log('---> getRegistryRef', self.database.ref(self.regDirectory + '/' + self.deviceType).toString())
		return self.database.ref(self.regDirectory + '/' + self.deviceType)
	}
	var getDataRef = function () {
		console.log('---> getRegistryRef', self.database.ref(self.dataDirectory).toString())
		return self.database.ref(self.dataDirectory)
	}

	// New methods:
	_extend(self, {

		// 'setAddress': function (deviceAddr) {
		// 	self.deviceAddr = deviceAddr;
		// 	self._writeLocal(self.storageKeyForDeviceAddr, deviceAddr);
		// },

		// 'resetAddress': function () {
		// 	if (self.getState() === 'state-offline') return Promise.reject('state-offline');
		// 	if (self.getAddress() == null)
		// 	return self.
		// 	self.setAddress(null);
		// },

		'getDataRef': getDataRef,
		'getRegistryRef': getRegistryRef,
		'getBusRef': getBusRef,

		/*
		Utilities for observation
			*/

		'startObserveBus': function (callback) {
			console.log("Starting PnP observer for bus...")
			console.log('device type: ', self.deviceType)
			console.log('PnP bus:', self.busDirectory)

			getBusRef().off()
			// Setup listener:
			getBusRef().on('value', function(snapshot){
				var value = snapshot.val() || {};
				callback(value)
			})
		},
		'stopObserveBus': function(callback) {
			getBusRef().off()
		},
		'startObserveRegistry': function(callback) {
			console.log("Starting PnP observer for Registry...")
			console.log('device type: ', self.deviceType)
			console.log('Registry:', self.regDirectory)

			getRegistryRef().off()
			// Setup listener:
			getRegistryRef().on('value', function(snapshot){
				var value = snapshot.val() || {};
				callback(value)
			})
		},
		'stopObserveRegistry': function() {
			getRegistryRef().off('value')
		},
		'startObserveData': function(callback) {
			console.log("Starting PnP observer for Data...")
			console.log('device type: ', self.deviceType)
			console.log('Data:', self.dataDirectory)

			getDataRef().off()
			// Setup listener:
			getDataRef().on('value', function(snapshot){
				var value = snapshot.val() || {};
				callback(value)
			})
		},
		'stopObserveData': function() {
			getDataRef().off('value')
		}

	})


	return self;

}

module.exports = PnPManager;