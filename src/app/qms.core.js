// this version uses firebase SDK
if (typeof 'dbi' === 'undefined') {
	throw new Error('"qms.core" module requires databse interface instance!');
}

var 
	module = {}

module.DEVICE_ENDPOINT_URI = '{office}/devices'

/**
 * Create pseudo-unique global identifier
 * (receipt from: http://stackoverflow.com/a/13403498)
 * @param {string} prefix Optional prefix for identifier
 */
function UID = function (prefix) {
	prefix = prefix || '';
	return prefix+Math.random().toString(36).substring(2, 15) +
		Math.random().toString(36).substring(2, 15);
}

/**
 * Base class to create persistent objects which can be reflectd in DB
 * @param {object} data Initialization data (optional)
 */
var PersistentItem = function (data) {}

PersistentItem.prototype.toJS = function() {
	// Using a simple trick how to extract only properties, not a functions:
	// JSON.serialize creates a string where data encoded in JSON format: '{'attribute':value}', functions are ignored as they are prohibited in JSON
	// The we perform the backward transform from a string to JavaScript object - now it contains only a data without a functions!
	var jsonEncoded = JSON.serialize(this);
	var jsObject = JSON.parse(jsonEncoded);
	return jsObject;
};

PersistentItem.prototype.fromJS(data) {
	for (var name in data) {
		this[name] = data[name]
	}
}
// Class-wide constant
PersistentItem.ENDPOINT_NAME = 'replase this value in descendants!'

// CRUD methods: Create, read, update, delete 
PersistentItem.prototype.create = function() {
	var ref = dbi.ref(this.ENDPOINT_NAME + '/'+this.id);
	return ref.set();
};
PersistentItem.prototype.read = function(attrName) {
	var ref = dbi.ref(this.ENDPOINT_NAME + '/'+this.id);
	return ref.set();
};
PersistentItem.prototype.update = function() {
	var ref = dbi.ref(this.ENDPOINT_NAME + '/'+this.id);
	return ref.set();
};
PersistentItem.prototype.delete = function(attrName) {
	var ref = dbi.ref(this.ENDPOINT_NAME + '/'+this.id);
	return ref.set();
};
PersistentItem.prototype.listen = function(callOnChanges, attrName) {
	if (typeof callOnChanges === 'function') {
		
	} else {
		throw new Error('"callOnChanges" must be a function!')
	}
};

var Display = function (data) {
	// Call "parent" constructor to add inherited properties/methods:
	PersistentItem.call(this, data); 
	// "data" can be optional (undefined), so allow to keep it empty:
	data = data || {}; // <- becomes an empty object when is null or undefined
	this.id = data.id || UID();
}


// CRUD methods: Create, read, update, delete 

/**
 * Create ("save") a new display record in a database
 * @return {Promise}                [description]
 */
Display.prototype.create = function() {
	var ref = dbi.ref(module.DEVICE_ENDPOINT_URI + '/'+this.id);
	return ref.set();
};

var Observer = function (data) {
	/*
	Add timer X
	Time delay measurement:
	write new random value, start timing,
	wait until FB notification about value changed
	measure time.
	Set updated delay value;
	Also - measure jitter, Min, Max delay, Average delay.
	 */
}