/*
To-do:

local storage
 */
*/
 */
/*
Firebase config for project:


<script src="https://www.gstatic.com/firebasejs/3.6.4/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAViQOYqEv34oDa5KMiP3l7a3cM0MzKXhY",
    authDomain: "qms-database-42b86.firebaseapp.com",
    databaseURL: "https://qms-database-42b86.firebaseio.com",
    storageBucket: "qms-database-42b86.appspot.com",
    messagingSenderId: "134682369542"
  };
  firebase.initializeApp(config);
</script>

 */

function DataServices(rootPath) {
	this.rootPath = rootPath;

	this.db = firebase
	this.auth = firebase.auth();
	this.database = firebase.database();
	this.storage = firebase.storage();
	// Initiates Firebase auth and listen to auth state changes.
	// this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));

	this.signIn = function (usr, pwd) {
		// Sign in Firebase using already created profile in Firebase.
		return this.auth.signInWithEmaiAndPassword(usr, pwd)
	}

	this.signOut = function() {
		// Sign out of Firebase.
		this.auth.signOut();
	}



}

function SystemEvent() {
	id
	timestamp
	entity
	state
}

Ticket: {id, displayText, tags:{}, state: 'call'}

//"Заявка" - ticket - это талон, билет, в наше случае талон не печатаем, в нашем случае - это "электронный билет", с уникальным идентификатором.
function Ticket() {
	id
	created
	stateWait
	stateCalling
	stateCustomerSession
	stateDone
}

//"Очередь" - коллекция заявок.
function TicketsQueue() {
	// body...
}

function ManagerProfile() {}

function AdminProfile() {}

function CustomerProfile() {}

===
function QMSApp (){}

var
	ST_WAITING_SERVER_RESPONSE
	ST_SIGNED_OUT
	ST_LOCKED
	ST_READY
	ST_CALLING
	ST_CUSTOMER_SESSION


var cssClassByState = {}
	ST_WAITING_SERVER_RESPONSE = 'state-waiting-response',
	ST_SIGNED_OUT:
	ST_LOCKED
	ST_READY
	ST_CALLING
	ST_CUSTOMER_SESSION
}


// Используем класс тела документа одновременно для управления отображением UI и хранения текущего стейта. Соглашение - других классов кроме "стейта" этому элементу не присваиваем.
var getConsoleState = function () {
	return document.getElementsByTagName('body')[0].class
}
var setConsoleState = function (stateCode) {
	document.getElementsByTagName('body')[0].class = stateCode
}

ManagerConsole.states = ['state-waiting-response', 'state-locked', 'state-ready', 'state-calling', 'state-customer-session']

function ManagerConsole(config) {

	stateSignOut
	stateLocked

	stateReady
	stateCalling
	stateCustomerSession

	init
	signIn
	signOut
	callTicket
	cancelCall
	startCustomerSession
	stopCustomerSession

	displayId

	checkDisplayState
	traceDisplay

	// assignDisplay: if display is offline - see its ID and assign it manually.

}

DisplayConsole.prototype.POSSIBLE_STATES = ['state-idle', 'state-ready', 'state-calling', 'state-calling','state-customer-session','state-maintenance', 'state-error', 'state-signed-off']


var setAppState = function (stateCode) {
	document.getElementById('#i-stateful').className = stateCode;
}

var getAppState = function () {
	return document.getElementById('#i-stateful').className
}

// This entity is read-only?

DisplayConsole.ENDPOINT_ADDR = ''
// content can be: text, html; ...
// field "lastOnline" - to clear old records (from another consoles - Manager, Admin?)
function DisplayConsole(config) {

	this.displayId =  null;

	this.db = firebase;
	this.auth = firebase.auth();
	this.database = firebase.database();
	this.storage = firebase.storage();
	// Initiates Firebase auth and listen to auth state changes.
	// this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));

	this.signIn = function (usr, pwd) {
		// Sign in Firebase using already created profile in Firebase.
		return this.auth.signInWithEmaiAndPassword(usr, pwd)
	}

	this.signOut = function() {
		// Sign out of Firebase.
		this.auth.signOut();
	}


	var ACK = function () {
		// body...
	}

	var setState = function (stateCode) {
		if (self.POSSIBLE_STATES.indexOf(stateCode) < 0)
			throw new Error('Invalid state!');
		setAppState(stateCode)
	}

	// stateIdle
	// stateReady
	// stateCalling
	// stateCustomerSession
	// stateMaintenance
	// stateError

	// init

	// displayId

	var displayDbRef;

	// to-do: auto trace mode. in case when no "id" in a local storage - go to the trace mode.
	// detectCaps: write capabilities in DB ...
	//

	var interval = 5 * 1000; //  sec

	var lastChangesTimestamp;

	function runTimer() {
		var now = (new Date()).getTime();
		setTimeout(runTimer, interval - now % interval)
	}

	var handleState = function (data) {
		var timestamp = (new Date()).getTime();
		document.getElementById('#i-stateful').innerHTML = data.content;
		setAppState(data.state);
		displayDbRef.child('ACK').set(timestamp)
		lastChangesTimestamp = timestamp;
	}

	var onAuthStateChanged = function (user) {
		if (user) {
			displayDbRef = database.ref('displays').child(displayId);
			displayDbRef.child('buffer').on('child_added', handleState)
			displayDbRef.child('buffer').on('child_changed', handleState)
		} else {
			displayDbRef = null;
			setAppState('state-signed-off')
		}
	}

	var init = function () {
		this.auth.onAuthStateChanged(onAuthStateChanged);
		this.dislpayId = 100;
		this.auth.signInWithEmailAndPassword('display@qms.app', 'passwd')
			.catch(function (reason) {
				console.error('Auth error: ', reason)
			});
		// self.displayId = localStorage.getItem('QMS_displayId') || null;
		// if (self.displayId) {
		// 	var
		// } else {
		// 		..
		// 	// Новый дисплей, не зарегистрирован в системе
		// 	setAppState('state-trace')
		// }
		// runTimer()
	}

	init()

}

function TicketConsole(config) {
	// +idle timer
	stateReady
	stateCustomerSession
	stateWaitingServerResponse
	stateOutputTicket

	reset
	outputTicket
}

function AdminConsole(config) {
	addUserAccount
	addManagerConsole
	addDisplayConsole
	addTicketConsole
	bindConsoles

	stateSignOut
	stateSignedIn
	stateWaitingServerResponse
}
