var firebase = require('firebase');

var config = {
    apiKey: "AIzaSyAViQOYqEv34oDa5KMiP3l7a3cM0MzKXhY",
    authDomain: "qms-database-42b86.firebaseapp.com",
    databaseURL: "https://qms-database-42b86.firebaseio.com",
    storageBucket: "qms-database-42b86.appspot.com",
    messagingSenderId: "134682369542"
};

firebase.initializeApp(config);

module.exports = firebase;